import "reflect-metadata";
import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";

import resolvers from "./controllers";
import { userMiddleware } from "./middleware/Users";
import { db } from "./db";
import { GQLCtx } from "./middleware/ts/gql";
import { Request } from "./middleware/ts/express";

async function main() {
  await db.$connect();
  const app = express();

  app.use(cors({ credentials: true, origin: true }));

  const schema = await buildSchema({ resolvers, validate: false });
  const server = new ApolloServer({
    context: ({ req, res }): GQLCtx => ({ req, res, user: (req as Request).user, prisma: db }),
    schema,
  });

  app.use(cookieParser());
  app.use(userMiddleware);

  server.applyMiddleware({ app, cors: false });

  app.listen(4000, () => {
    console.log("Listening on http://localhost:4000");
  });
}

main();
