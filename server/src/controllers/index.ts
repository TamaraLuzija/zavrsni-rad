import { AuthResolver } from "./Auth/resolver";
import { UserResolver } from "./Auth/res";
import { GroupsResolver } from "./Groups/resolver";
import { GroupResolver, InvitationResolver } from "./Groups/res";
import { SettingsResolve } from "./Settings/resolver";
import { UsersResolver } from "./Users/resolver";
import { ExercisesResolver } from "./Exercises/resolver";
import { GoalsResolver, UserAchievementTypeResolver } from "./Achievements/resolver";
import { ActivitiesResolver, ActivityResolver } from "./Activities/resolver";
import { ProfileResolver } from "./Profile/resolver";

const resolvers = [
  AuthResolver,
  SettingsResolve,
  UserResolver,
  UsersResolver,
  GroupResolver,
  GroupsResolver,
  InvitationResolver,
  ExercisesResolver,
  GoalsResolver,
  ActivityResolver,
  ActivitiesResolver,
  UserAchievementTypeResolver,
  ProfileResolver,
] as const;

export default resolvers;
