import { Ctx, Field, Int, ObjectType, Query, Resolver } from "type-graphql";
import { addDays, startOfWeek } from "date-fns";

import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";

@ObjectType()
class Stat {
  @Field(() => Int)
  now: number;

  @Field(() => Int)
  before: number;

  @Field(() => Int, { nullable: true })
  countNow: number;

  @Field(() => Int, { nullable: true })
  countBefore: number;
}

@ObjectType()
class ProfileStats {
  @Field(() => Stat)
  work: Stat;

  @Field(() => Stat)
  break: Stat;

  @Field(() => Int)
  diff: number;

  @Field(() => Int)
  diffBefore: number;
}

export const sum = (all: number, curr: { time: number }) => {
  return all + curr.time;
};

export const week = (weekStart: Date) => ({
  AND: [{ date: { gt: startOfWeek(weekStart) } }, { date: { lte: addDays(weekStart, 7) } }],
});

export const getData = async (id: number, weekStart: Date) => {
  const weekRange = week(weekStart);
  const data = await db.activity.findMany({ where: { user: { id }, ...weekRange } });
  const works = data.filter((a) => a.isBreak === false);
  const breaks = data.filter((a) => a.isBreak === true);

  return {
    work: works.reduce(sum, 0),
    workCount: works.length,
    break: breaks.reduce(sum, 0),
    breakCount: breaks.length,
    diff: Array.from(new Set(data.map((a) => a.exercisesId).filter(Boolean))).length,
  };
};

@Resolver()
export class ProfileResolver {
  @Query(() => ProfileStats)
  async profileStats(@Ctx() ctx: GQLCtx): Promise<ProfileStats> {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const today = new Date();
    const mon = startOfWeek(today, { weekStartsOn: 1 });

    const thisWeek = await getData(ctx.user.id, mon);
    const lastWeek = await getData(ctx.user.id, addDays(mon, -7));

    return {
      work: {
        before: lastWeek.work,
        countBefore: lastWeek.workCount,
        now: thisWeek.work,
        countNow: thisWeek.workCount,
      },
      break: {
        before: lastWeek.break,
        countBefore: lastWeek.breakCount,
        now: thisWeek.break,
        countNow: thisWeek.breakCount,
      },
      diff: thisWeek.diff,
      diffBefore: lastWeek.diff,
    };
  }
}
