import { UserAchievementTypeType, UserAchievementType } from "@prisma/client";
import { InputType, Field, Int, ObjectType } from "type-graphql";

@InputType()
export class NewGoalInput {
  @Field(() => Int)
  typeId: number;
}
