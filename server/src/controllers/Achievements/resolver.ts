import {
  Arg,
  Ctx,
  Field,
  FieldResolver,
  Int,
  Mutation,
  ObjectType,
  Query,
  Resolver,
  Root,
} from "type-graphql";
import {
  UserAchievementType,
  UserAchievement,
  UserAchievementTypeType,
} from "@generated/type-graphql";
import { db } from "../../db";
import { NewGoalInput } from "./inputs";
import { GQLCtx } from "../../middleware/ts/gql";
import { UserAchievementTypeType as PrismaTypeThingy } from "@prisma/client";
import { achievementNames } from "../../constants/achivementNames";

@Resolver((of) => UserAchievementType)
export class UserAchievementTypeResolver {
  @FieldResolver(() => String)
  async typeName(@Root() root: UserAchievementType) {
    return achievementNames[root.type];
  }
}

@ObjectType()
class CompletionItem {
  @Field(() => UserAchievementTypeType)
  type: UserAchievementTypeType;

  @Field(() => Int)
  value: number;
}

@ObjectType()
class UserAchievementsQueryResponse {
  @Field(() => [UserAchievementType])
  types: UserAchievementType[];

  @Field(() => [CompletionItem])
  userCompletedTypes: CompletionItem[];

  @Field(() => [CompletionItem])
  allTypes: CompletionItem[];
}

@ObjectType()
class DataResponse {
  @Field(() => String)
  id: string;

  @Field(() => String)
  name: string;

  @Field(() => Int)
  maxLevel: number;

  @Field(() => Int, { nullable: true })
  userLevel?: number;

  @Field(() => Int, { nullable: true })
  currentValue?: number;

  @Field(() => Int, { nullable: true })
  nextValue?: number;

  @Field(() => String, { nullable: true })
  message?: string;
}

// TODO: Extract this and merge with activities/resolver.ts
const countFunc = (id: number, isBreak: boolean) => async () => {
  const res = await db.activity.aggregate({
    sum: { time: true },
    where: { isBreak, usersId: id },
  });

  return res.sum.time || 0;
};

const getCurrent = (id: number, key: PrismaTypeThingy) => {
  switch (key) {
    case "countWork":
      return db.activity.count({ where: { user: { id }, isBreak: false } });
    case "countBreak":
      return db.activity.count({ where: { user: { id }, isBreak: true } });
    case "countExercises":
      return db.exercise.count({ where: { user: { id } } });
    case "timeWork":
      return countFunc(id, false)();
    case "timeBreak":
      return countFunc(id, true)();
    case "countCreateGroup":
      return db.group.count({ where: { owner: { id } } });
    case "joinGroup":
      return db.group.count({ where: { users: { some: { id } }, ownerId: { not: id } } });
  }
};

@Resolver()
export class GoalsResolver {
  @Query(() => [DataResponse], { nullable: true })
  async userAchievements(@Ctx() ctx: GQLCtx): Promise<DataResponse[]> {
    if (!ctx.user) {
      throw new Error("Not logged in");
    }

    const allTypes = await db.userAchievementType.findMany({
      include: { userAchievement: { where: { userId: ctx.user.id } } },
    });

    const keys = Object.keys(PrismaTypeThingy) as PrismaTypeThingy[];
    const groupByKey = keys.map(
      (key) =>
        [key, allTypes.filter((t) => t.type === key).sort((a, b) => a.value - b.value)] as const
    );

    const output: DataResponse[] = [];
    for (const [key, achievementTypes] of groupByKey) {
      const lastAchievementWithUser = achievementTypes
        .map((t, i) => (t.userAchievement.length === 1 ? ([t, i] as const) : undefined))
        .filter(Boolean)
        .reverse()[0];
      const firstWithoutUser = achievementTypes
        .map((t, i) => (t.userAchievement.length === 0 ? ([t, i] as const) : undefined))
        .filter(Boolean)[0];

      output.push({
        id: key,
        name: achievementNames[key],
        userLevel: lastAchievementWithUser?.[1],
        currentValue: await getCurrent(ctx.user.id, key),
        message: lastAchievementWithUser?.[0].name,
        nextValue: firstWithoutUser?.[0].value,
        maxLevel: achievementTypes.length,
      });
    }

    return output;
  }

  @Query(() => UserAchievementsQueryResponse, { nullable: true })
  async userAchievementType(@Ctx() ctx: GQLCtx): Promise<UserAchievementsQueryResponse> {
    if (!ctx.user) {
      throw new Error("Not logged in");
    }

    const achieved = await db.userAchievement.findMany({
      where: { userId: ctx.user.id },
      orderBy: { date: "desc" },
      include: { type: true },
    });

    const userAchievements: Partial<Record<PrismaTypeThingy, number>> = {};
    const userCompleted: Partial<Record<PrismaTypeThingy, number>> = {};

    achieved.forEach((achievement) => {
      if ((userAchievements[achievement.type.type] || -1) < achievement.type.value) {
        userAchievements[achievement.type.type] = achievement.type.value;
      }

      if (!userCompleted[achievement.type.type]) {
        userCompleted[achievement.type.type] = 0;
      }

      // @ts-ignore
      userCompleted[achievement.type.type]++;
    });

    const types = await db.userAchievementType.findMany({
      orderBy: [{ type: "asc" }, { value: "asc" }],
    });

    const allData: Partial<Record<PrismaTypeThingy, number>> = {};
    types.forEach((type) => {
      if (!allData[type.type]) {
        allData[type.type] = 0;
      }

      // @ts-ignore
      allData[type.type]++;
    });

    const tmp = types.filter((type) => {
      if ((userAchievements[type.type] || -1) < type.value) {
        return true;
      }

      return false;
    });

    const output = [];
    const seenTypes: PrismaTypeThingy[] = [];

    for (let i = 0; i < tmp.length; i++) {
      if (seenTypes.includes(tmp[i].type)) {
        continue;
      }

      output.push(tmp[i]);
      seenTypes.push(tmp[i].type);
    }

    return {
      types: output,
      userCompletedTypes: Object.entries(userCompleted).map(
        ([type, value]) => ({ type: type, value } as any)
      ),
      allTypes: Object.entries(allData).map(([type, value]) => ({ type: type, value } as any)),
    };
  }

  @Mutation(() => UserAchievement)
  async createActivity(@Ctx() ctx: GQLCtx, @Arg("data") data: NewGoalInput) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    //TODO:
    // create activity

    // select all that can change if new activity

    // foreach if reached and user not have -> create

    // return new achievement + activity

    return db.userAchievement.create({
      data: {
        date: new Date(),
        type: {
          connect: {
            id: data.typeId,
          },
        },
        user: {
          connect: {
            id: ctx.user.id,
          },
        },
      },
    });
  }
}
