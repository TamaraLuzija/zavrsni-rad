import {
  Ctx,
  Int,
  Arg,
  Mutation,
  Resolver,
  InputType,
  Field,
  Query,
  FieldResolver,
  Root,
} from "type-graphql";
import { Activity, Exercise } from "@generated/type-graphql";

import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";
import { checkForAchievement } from "../../lib/achievements";
@InputType()
class UpdateActivityType {
  @Field(() => Boolean, { nullable: true })
  isBreak?: boolean;

  @Field(() => Boolean, { nullable: true })
  isComplete?: boolean;

  @Field(() => Int, { nullable: true })
  exerciseId?: number;

  @Field(() => Int, { nullable: true })
  time?: number;

  @Field(() => Int, { nullable: true })
  fullTime?: number;
}

@Resolver(() => Activity)
export class ActivityResolver {
  @FieldResolver(() => Exercise, { nullable: true })
  async exercise(@Root() root: Activity & { exercise: Exercise }) {
    if (root.exercise !== undefined) {
      return root.exercise;
    }

    if (root.exercisesId) {
      return db.exercise.findUnique({ where: { id: root.exercisesId } });
    }

    return null;
  }
}

@Resolver()
export class ActivitiesResolver {
  @Query(() => [Activity])
  async activities(@Ctx() ctx: GQLCtx) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    return db.activity.findMany({ take: 50, orderBy: { id: "desc" }, include: { exercise: true } });
  }

  @Mutation(() => Activity)
  async createActivity(
    @Ctx() ctx: GQLCtx,
    @Arg("isBreak", () => Boolean) isBreak: boolean,
    @Arg("fullTime", () => Int) fullTime: number
  ) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const activity = await db.activity.create({
      data: {
        isBreak,
        fullTime,
        user: { connect: { id: ctx.user.id } },
      },
    });

    const countFunc = (isBreak: boolean) => () => {
      return db.activity.count({ where: { isBreak } });
    };

    await checkForAchievement(ctx.user.id, "countWork", countFunc(false));
    await checkForAchievement(ctx.user.id, "countBreak", countFunc(true));

    return activity;
  }

  @Mutation(() => Activity)
  async updateActivity(
    @Ctx() ctx: GQLCtx,
    @Arg("id", () => Int) id: number,
    @Arg("data", () => UpdateActivityType) data: UpdateActivityType
  ) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    let activity = await db.activity.findUnique({ where: { id } });
    if (!activity) {
      throw new Error("Activity not found");
    }

    const { isComplete, ...rest } = data;

    activity = await db.activity.update({
      where: { id },
      data: {
        ...rest,
        complete: isComplete,
      },
    });

    const countFunc = (isBreak: boolean) => async () => {
      const res = await db.activity.aggregate({
        sum: { time: true },
        where: { isBreak, usersId: ctx.user?.id },
      });

      return res.sum.time || 0;
    };

    await checkForAchievement(ctx.user.id, "timeWork", countFunc(false));
    await checkForAchievement(ctx.user.id, "timeBreak", countFunc(true));

    return activity;
  }
}
