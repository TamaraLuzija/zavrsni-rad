import { Arg, Field, FieldResolver, Int, ObjectType, Resolver, Root } from "type-graphql";
import { Group, Invitation, User, Exercise } from "@generated/type-graphql";
import { db } from "../../db";
import { endOfWeek, startOfWeek } from "date-fns";
import { getData } from "../Profile/resolver";

@ObjectType()
class Stats {
  @Field(() => Int)
  work: number;

  @Field(() => Int)
  break: number;
}

@Resolver((of) => User)
export class UserResolver {
  @FieldResolver(() => [Group])
  async groups(@Root() root: User & { groups: Group[] }) {
    if (root.groups !== undefined) {
      return root.groups;
    }

    return db.group.findMany({
      where: { users: { some: { id: root.id } } },
      include: { users: true },
    });
  }

  @FieldResolver(() => Stats)
  async stats(@Root() root: User): Promise<Stats> {
    const data = await getData(root.id, startOfWeek(new Date(), { weekStartsOn: 1 }));
    return {
      break: data.break,
      work: data.work,
    };
  }

  @FieldResolver(() => [Invitation])
  async invitations(
    @Root() root: User & { invitations: Invitation[] },
    @Arg("joined", () => Boolean, { nullable: true }) joined?: boolean
  ) {
    if (root.invitations !== undefined) {
      return root.invitations;
    }

    return db.invitation.findMany({
      where: { userId: root.id, joinedAt: joined === false ? null : undefined },
    });
  }

  @FieldResolver(() => [Exercise])
  async exercises(@Root() root: User & { exercises: Exercise[] }) {
    if (root.exercises !== undefined) {
      return root.exercises;
    }

    return db.exercise.findMany({ where: { userId: root.id } });
  }
}
