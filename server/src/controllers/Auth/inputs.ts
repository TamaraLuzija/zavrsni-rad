import { InputType, Field, ObjectType } from "type-graphql";
import { User } from "@generated/type-graphql";

@InputType()
export class SignUpInput {
  @Field()
  email: string;

  @Field()
  username: string;

  @Field()
  password: string;
}

@ObjectType()
export class AuthenticateType {
  @Field(() => String)
  token: string;

  @Field(() => User)
  user: User;
}
