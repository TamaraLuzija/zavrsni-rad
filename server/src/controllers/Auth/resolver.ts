import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { Resolver, Arg, Mutation, Query, Ctx, FieldResolver, Root } from "type-graphql";

import { User, Group, Invitation } from "@generated/type-graphql";
import { AuthenticateType, SignUpInput } from "./inputs";
import { hash } from "../../lib/hash";
import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";

@Resolver()
export class AuthResolver {
  @Query(() => User, { nullable: true })
  async me(@Ctx() ctx: GQLCtx) {
    if (!ctx.user?.id) {
      return null;
    }

    return db.user.findUnique({
      where: { id: ctx.user?.id },
      include: {
        exercises: { orderBy: { createdAt: "desc" } },
        groups: {
          include: {
            users: true,
            invitations: {
              include: {
                user: true,
              },
            },
          },
        },
      },
    });
  }

  @Mutation(() => AuthenticateType)
  async signUp(@Arg("data") data: SignUpInput): Promise<AuthenticateType> {
    const { username, email, password } = data;
    const lowerEmail = email.toLowerCase();
    const hashedPassword = await hash(password);

    let user = await db.user.findUnique({ where: { email: lowerEmail } });
    if (user) {
      throw new Error("EMAIL_IN_USE/Email already in use");
    }

    user = await db.user.create({ data: { email: lowerEmail, hashedPassword, username } });
    const token = jwt.sign({ id: user.id }, "OVO_JE_TOKEN");

    return { user, token };
  }

  @Mutation(() => AuthenticateType)
  async signIn(
    @Arg("email") email: string,
    @Arg("password") password: string
  ): Promise<AuthenticateType> {
    const user = await db.user.findUnique({ where: { email } });

    if (!user) {
      throw new Error("EMAIL_NOT_FOUND/Cant find user with that email");
    }

    const valid = await bcrypt.compare(password, user.hashedPassword);

    if (!valid) {
      throw new Error("INVALID_PASSWORD/Incorrect password");
    }

    const token = jwt.sign({ id: user.id }, "OVO_JE_TOKEN");
    return { token, user };
  }
}
