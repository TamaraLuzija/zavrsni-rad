import { Ctx, Query, Resolver } from "type-graphql";

import { User } from "@generated/type-graphql";
import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";

@Resolver()
export class UsersResolver {
  @Query(() => [User], { nullable: true })
  async users(@Ctx() ctx: GQLCtx) {
    if (!ctx.user) {
      throw new Error("not logged in");
    }

    return db.user.findMany();
  }
}
