import { Arg, Ctx, Mutation, Resolver } from "type-graphql";
import { User } from "@generated/type-graphql";
import { TimePreferenceInputs, QuestionsInputs } from "./inputs";
import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";

@Resolver()
export class SettingsResolve {
  @Mutation(() => User)
  async updateTime(@Ctx() ctx: GQLCtx, @Arg("data") data: TimePreferenceInputs) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    return db.user.update({
      where: { id: ctx.user.id },
      data: { ...data },
    });
  }

  @Mutation(() => User)
  async updateQuestions(@Ctx() ctx: GQLCtx, @Arg("data") data: QuestionsInputs) {
    if (!ctx.user) {
      throw new Error("Not logged in");
    }

    return db.user.update({
      where: { id: ctx.user.id },
      data: { ...data },
    });
  }
}
