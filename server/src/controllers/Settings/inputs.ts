import { InputType, Field, Int } from "type-graphql";

@InputType()
export class TimePreferenceInputs {
  @Field(() => Int, { nullable: true })
  timePreference?: number;

  @Field(() => Int, { nullable: true })
  breakLength?: number;
}
@InputType()
export class QuestionsInputs {
  @Field(() => Int, { nullable: true })
  timePreference?: number;

  @Field(() => Int, { nullable: true })
  breakLength?: number;

  @Field({ nullable: true })
  typeOfExercise?: string;
}
