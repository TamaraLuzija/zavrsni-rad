import { InputType, Field, Int } from "type-graphql";

@InputType()
export class NewExInput {
  @Field(() => String)
  name: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String,  { nullable: true })
  link?: string;
}

@InputType()
export class UpdateExInput {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String,  { nullable: true })
  link?: string;
}
