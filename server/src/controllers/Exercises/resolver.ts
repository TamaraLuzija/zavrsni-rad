import { Arg, Ctx, Int, Mutation, Resolver } from "type-graphql";
import { Exercise } from "@generated/type-graphql";

import { db } from "../../db";
import { NewExInput, UpdateExInput } from "./inputs";
import { GQLCtx } from "../../middleware/ts/gql";
import { checkForAchievement } from "../../lib/achievements";

@Resolver()
export class ExercisesResolver {
  @Mutation(() => Exercise)
  async createExercise(@Ctx() ctx: GQLCtx, @Arg("data") data: NewExInput) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const { id } = ctx.user;
    await checkForAchievement(id, "countExercises", () =>
      db.exercise.count({ where: { userId: id } })
    );

    return db.exercise.create({
      data: {
        ...data,
        user: {
          connect: {
            id: ctx.user.id,
          },
        },
      },
    });
  }

  @Mutation(() => Exercise)
  async updateExercise(
    @Ctx() ctx: GQLCtx,
    @Arg("id", () => Int) id: number,
    @Arg("data") data: UpdateExInput
  ) {
    if (!ctx.user) {
      throw new Error("Not logged in");
    }

    const exercise = await db.exercise.findUnique({ where: { id } });
    if (!exercise || exercise.userId !== ctx.user.id) {
      throw new Error("Not found");
    }

    return db.exercise.update({
      where: { id },
      data: { ...data },
    });
  }

  @Mutation(() => Exercise)
  async deleteExercise(@Ctx() ctx: GQLCtx, @Arg("id", () => Int) id: number) {
    if (!ctx.user) {
      throw new Error("Not logged in");
    }

    const exercise = await db.exercise.findUnique({ where: { id } });
    if (!exercise || exercise.userId !== ctx.user.id) {
      throw new Error("Not found");
    }

    return db.exercise.delete({
      where: { id },
    });
  }
}
