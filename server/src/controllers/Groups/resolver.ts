import { Arg, Ctx, Int, Mutation, Query, Resolver } from "type-graphql";
import { Invitation as PrismaInvitation, User as PrismaUser } from "@prisma/client";
import { Group, User } from "@generated/type-graphql";

import { GroupInput } from "./inputs";
import { db } from "../../db";
import { GQLCtx } from "../../middleware/ts/gql";
import { checkForAchievement } from "../../lib/achievements";
import { mailerService } from "../../lib/mail";

export type FullGroup = Group & {
  users: PrismaUser[];
  invitations: (PrismaInvitation & { user: PrismaUser })[];
  owner: PrismaUser;
};

const include = {
  invitations: {
    include: {
      user: true,
    },
  },
  users: true,
  owner: true,
};

@Resolver()
export class GroupsResolver {
  @Query(() => [Group])
  groups() {
    return db.group.findMany({ include });
  }

  @Mutation(() => Boolean)
  async respondToInvite(
    @Ctx() ctx: GQLCtx,
    @Arg("inviteId", () => Int) inviteId: number,
    @Arg("accept", () => Boolean) accept: boolean
  ) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const invite = await db.invitation.findUnique({ where: { id: inviteId } });
    if (!invite) {
      throw new Error("Invite not found");
    }

    if (invite.joinedAt) {
      throw new Error("Already joined");
    }

    if (accept) {
      await db.$transaction([
        db.invitation.update({ where: { id: inviteId }, data: { joinedAt: new Date() } }),
        db.group.update({
          where: { id: invite.groupId },
          data: { users: { connect: { id: ctx.user.id } } },
        }),
      ]);

      const { id } = ctx.user;
      await checkForAchievement(id, "joinGroup", () =>
        db.group.count({ where: { users: { some: { id } } } })
      );

      return true;
    }

    await db.invitation.delete({ where: { id: inviteId } });
    return true;
  }

  @Mutation(() => Group)
  async createGroup(@Ctx() ctx: GQLCtx, @Arg("data") data: GroupInput): Promise<FullGroup> {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const { name, memberIds } = data;
    const group = await db.group.create({
      data: {
        name,
        invitations: { create: memberIds.map((id) => ({ user: { connect: { id } } })) },
        owner: { connect: { id: ctx.user.id } },
        users: { connect: { id: ctx.user.id } },
      },
      include,
    });

    await Promise.all(
      group.invitations.map((invite) =>
        mailerService.inviteMail(invite.user.email, name, ctx.user!.username || ctx.user!.email)
      )
    );

    await checkForAchievement(ctx.user.id, "countCreateGroup", () =>
      db.group.count({ where: { ownerId: ctx.user!.id } })
    );

    return group;
  }

  @Mutation(() => Group)
  async kickUser(
    @Ctx() ctx: GQLCtx,
    @Arg("groupId", () => Int) groupId: number,
    @Arg("userId", () => Int) userId: number
  ): Promise<Group> {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const group = await db.group.findUnique({
      where: { id: groupId },
      include: {
        users: true,
      },
    });

    if (!group) {
      throw new Error("Group not found");
    }

    if (group.ownerId !== ctx.user.id || group.ownerId === userId) {
      throw new Error("You don't have permissions to do that");
    }

    if (group.users.every((u) => u.id !== userId)) {
      throw new Error("User not in group");
    }

    await db.invitation.deleteMany({ where: { groupId, userId } });

    return db.group.update({
      where: { id: groupId },
      data: { users: { disconnect: { id: userId } } },
      include: { users: true },
    });
  }

  @Mutation(() => User)
  async leave(@Ctx() ctx: GQLCtx, @Arg("groupId", () => Int) groupId: number): Promise<User> {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const group = await db.group.findUnique({
      where: { id: groupId },
      include: {
        users: true,
      },
    });

    if (!group) throw new Error("Group not found");
    if (group.ownerId === ctx.user.id) throw new Error("You're an owner");
    if (group.users.every((u) => u.id !== ctx.user?.id)) {
      throw new Error("You're not in this group");
    }

    await db.invitation.deleteMany({ where: { groupId, userId: ctx.user.id } });

    await db.group.update({
      where: { id: groupId },
      data: { users: { disconnect: { id: ctx.user.id } } },
      include: { users: true },
    });

    const user = await db.user.findUnique({ where: { id: ctx.user.id } });
    if (!user) throw new Error("Error");

    return user;
  }

  @Mutation(() => Group)
  async inviteToGroup(
    @Ctx() ctx: GQLCtx,
    @Arg("id", () => Int) id: number,
    @Arg("userIds", () => [Int]) userIds: number[]
  ) {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    const group = await db.group.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!group) {
      throw new Error("Group not found");
    }

    if (group.users.every((u) => u.id !== ctx.user!.id)) {
      throw new Error("You're not a part of this group");
    }

    const users = await db.user.findMany({ where: { id: { in: userIds } } });
    if (users.length !== userIds.length) {
      throw new Error("Some users not found");
    }

    await db.$transaction(
      users.map((user) =>
        db.invitation.create({
          data: {
            group: { connect: { id } },
            user: { connect: { id: user.id } },
          },
        })
      )
    );

    await Promise.all(
      users.map((user) =>
        mailerService.inviteMail(user.email, group.name, ctx.user!.username || ctx.user!.email)
      )
    );

    return group;
  }

  @Mutation(() => Group)
  async deleteInvite(
    @Ctx() ctx: GQLCtx,
    @Arg("groupId", () => Int) groupId: number,
    @Arg("inviteId", () => Int) inviteId: number
  ): Promise<Group> {
    if (!ctx.user) {
      throw new Error("User not logged in");
    }

    let group = await db.group.findUnique({
      where: { id: groupId },
      include: {
        invitations: true,
      },
    });

    if (!group) {
      throw new Error("Group not found");
    }

    if (group.ownerId !== ctx.user.id) {
      throw new Error("You don't have permissions to do that");
    }

    if (group.invitations.every((u) => u.id !== inviteId)) {
      throw new Error("Invite not found");
    }

    await db.invitation.delete({ where: { id: inviteId } });
    return db.group.findUnique({ where: { id: groupId }, rejectOnNotFound: true });
  }
}
