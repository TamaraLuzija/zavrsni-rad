import { InputType, Field, Int } from "type-graphql";

@InputType()
export class GroupInput {
  @Field()
  name: string;

  @Field(() => [Int])
  memberIds: number[];
}
