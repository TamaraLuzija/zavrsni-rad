import { Arg, Field, FieldResolver, Int, ObjectType, Resolver, Root } from "type-graphql";
import { Group, User, Invitation } from "@generated/type-graphql";

import { FullGroup } from "./resolver";
import { db } from "../../db";
import { sum, week } from "../Profile/resolver";

@Resolver((of) => Invitation)
export class InvitationResolver {
  @FieldResolver((type) => User, { nullable: true })
  async user(@Root() invitation: FullGroup["invitations"][number]): Promise<User | undefined> {
    if (invitation.user !== undefined) {
      return invitation.user;
    }

    return (await db.user.findUnique({ where: { id: invitation.userId } })) || undefined;
  }

  @FieldResolver((type) => Group, { nullable: true })
  async group(@Root() invitation: FullGroup["invitations"][number]): Promise<Group | undefined> {
    if (invitation.group !== undefined) {
      return invitation.group;
    }

    return (await db.group.findUnique({ where: { id: invitation.groupId } })) || undefined;
  }
}

@ObjectType()
class UserWithRank extends User {
  sort: number;

  @Field(() => Int)
  rank: number;

  @Field(() => Int)
  groupId: number;
}

@Resolver((of) => Group)
export class GroupResolver {
  @FieldResolver((type) => [UserWithRank], { nullable: true })
  async usersWithRank(@Root() group: FullGroup): Promise<UserWithRank[] | undefined> {
    const users = await db.user.findMany({
      where: { groups: { some: { id: group.id } } },
      include: { activities: { where: { ...week(new Date()) } } },
    });

    const data = users
      .map((u) => ({
        ...u,
        count: {
          work: u.activities.filter((a) => a.isBreak === false).reduce(sum, 0),
          break: u.activities.filter((a) => a.isBreak === true).reduce(sum, 0),
        },
      }))
      .map((u) => ({ ...u, sort: u.count.work + u.count.break / 2 }));

    return data
      .sort((a, b) => b.sort - a.sort)
      .reduce((all, user) => {
        if (all.length === 0) {
          all.push({ ...user, rank: 1, groupId: group.id });
        } else {
          const last = all[all.length - 1];
          if (last.sort === user.sort) {
            all.push({ ...user, rank: last.rank, groupId: group.id });
          } else {
            all.push({ ...user, rank: last.rank + 1, groupId: group.id });
          }
        }

        return all;
      }, [] as UserWithRank[]);
  }

  @FieldResolver(() => [Invitation])
  async invitations(
    @Root() group: FullGroup,
    @Arg("joined", () => Boolean, { nullable: true }) joined?: boolean
  ) {
    return db.invitation.findMany({
      where: {
        group: { id: group.id },
        joinedAt: !joined ? null : undefined,
      },
    });
  }

  @FieldResolver((type) => User)
  async owner(@Root() group: FullGroup) {
    if (group.owner !== undefined) {
      return group.owner;
    }

    return db.user.findUnique({ where: { id: group.ownerId } });
  }
}
