import { PrismaClient, User } from "@prisma/client";
import { Request, Response } from "./express";

export interface GQLCtx {
  user?: User;
  res: Response;
  req: Request;
  prisma: PrismaClient;
}
