import { UserAchievementTypeType } from "@prisma/client";

export const achievementNames = {
  [UserAchievementTypeType.countWork]: "Work sessions",
  [UserAchievementTypeType.countBreak]: "Break sessions",
  [UserAchievementTypeType.timeWork]: "Work time (hours)",
  [UserAchievementTypeType.timeBreak]: "Break time (hours)",
  [UserAchievementTypeType.joinGroup]: "Groups joined",
  [UserAchievementTypeType.countExercises]: "Exercised created", // change this to countExercise
  [UserAchievementTypeType.countCreateGroup]: "Groups created",
};
