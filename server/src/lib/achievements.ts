import { db } from "../db";
import { UserAchievementTypeType } from ".prisma/client";

const createAchievement = (id: number, typeId: number, date = new Date()) => {
  return db.userAchievement.create({
    data: {
      date: new Date(),
      user: { connect: { id } },
      type: {
        connect: { id: typeId },
      },
    },
  });
};

export const checkForAchievement = async (
  id: number,
  type: UserAchievementTypeType,
  valueFunc: () => Promise<number>
) => {
  const userAchievement = await db.userAchievement.findFirst({
    where: { userId: id, type: { type } },
    include: { type: true },
    orderBy: { type: { value: "desc" } },
  });

  const count = await valueFunc();
  const achievementsAfter = await db.userAchievementType.findMany({
    where: { type, value: userAchievement ? { gt: userAchievement.type.value } : undefined },
    orderBy: { value: "asc" },
  });

  for (const achievement of achievementsAfter) {
    if (count >= achievement.value) {
      await createAchievement(id, achievement.id);
    }
  }
};
