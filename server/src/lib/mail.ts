import nodemailer, { Transporter } from "nodemailer";
import mailgun from "nodemailer-mailgun-transport";
import SMTPTransport from "nodemailer/lib/smtp-transport";

const dev = false;

class MailerService {
  transport: Transporter<SMTPTransport.SentMessageInfo>;

  constructor() {
    let args: SMTPTransport.Options;
    if (dev) {
      args = {
        host: "localhost",
        port: 1025,
        auth: {
          user: "project.1",
          pass: "secret.1",
        },
      };
    } else {
      const apiKey = process.env.MAILGUN_API_KEY;

      if (!apiKey) {
        throw new Error("Mailgun api key missing");
      }

      args = mailgun({
        auth: {
          apiKey,
          domain: "tamtam.tk",
        },
        host: "api.eu.mailgun.net",
      });
    }

    this.transport = nodemailer.createTransport(args);
  }

  async send(to: string, data: { subject: string; text?: string; html?: string }) {
    try {
      await this.transport.sendMail({
        to,
        from: "Standy <no-replay@tamtam.tk>",
        ...data,
      });
    } catch (e) {
      console.error(e);
    }
  }

  inviteMail(to: string, groupName: string, inviterName: string) {
    return this.send(to, {
      subject: `Standy: ${inviterName} invited you to "${groupName}"`,
      html: `<div style="text-align: center"><h1>Standy</h1><br/><h2>${inviterName} invited you to "${groupName}"</h2><br/><p>Open Standy on your computer to respond to this invite</p></div>`,
    });
  }
}

export const mailerService = new MailerService();
