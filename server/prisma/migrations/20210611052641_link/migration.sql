/*
  Warnings:

  - You are about to drop the column `color` on the `GroupAchievementType` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "GroupAchievementType" DROP COLUMN "color",
ADD COLUMN     "link" TEXT;
