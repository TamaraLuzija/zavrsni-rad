/*
  Warnings:

  - A unique constraint covering the columns `[groupId,userId]` on the table `Invitation` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Invitation.groupId_userId_unique" ON "Invitation"("groupId", "userId");
