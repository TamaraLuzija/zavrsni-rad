/*
  Warnings:

  - Added the required column `type` to the `UserAchievementType` table without a default value. This is not possible if the table is not empty.
  - Added the required column `value` to the `UserAchievementType` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "UserAchievementTypeType" AS ENUM ('countWork', 'countBreak', 'timeWork', 'timeBreak', 'countActivity', 'joinGroup', 'countCreateGroup');

-- AlterTable
ALTER TABLE "UserAchievementType" ADD COLUMN     "type" "UserAchievementTypeType" NOT NULL,
ADD COLUMN     "value" INTEGER NOT NULL;
