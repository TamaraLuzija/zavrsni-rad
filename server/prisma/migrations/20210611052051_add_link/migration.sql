/*
  Warnings:

  - You are about to drop the column `color` on the `Exercise` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Exercise" DROP COLUMN "color",
ADD COLUMN     "link" TEXT;
