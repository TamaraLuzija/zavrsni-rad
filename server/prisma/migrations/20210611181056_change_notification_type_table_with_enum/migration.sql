/*
  Warnings:

  - You are about to drop the column `typeId` on the `Notification` table. All the data in the column will be lost.
  - You are about to drop the `NotificationType` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `type` to the `Notification` table without a default value. This is not possible if the table is not empty.

*/

-- DropForeignKey
ALTER TABLE "Notification" DROP CONSTRAINT "Notification_typeId_fkey";

-- AlterTable
ALTER TABLE "Notification" DROP COLUMN "typeId";

-- DropTable
DROP TABLE "NotificationType";

-- CreateEnum
CREATE TYPE "NotificationType" AS ENUM ('Invitation', 'InvitationAccept', 'Achievement');
ALTER TABLE "Notification" ADD COLUMN     "type" "NotificationType" NOT NULL;
