/*
  Warnings:

  - You are about to drop the column `period` on the `Activity` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Activity" DROP COLUMN "period",
ADD COLUMN     "time" INTEGER NOT NULL DEFAULT 0;
