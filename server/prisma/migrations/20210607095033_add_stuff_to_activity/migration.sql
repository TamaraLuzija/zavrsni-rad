/*
  Warnings:

  - Made the column `period` on table `Activity` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Activity" ADD COLUMN     "complete" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "isBreak" BOOLEAN NOT NULL DEFAULT false,
ALTER COLUMN "exercisesId" DROP NOT NULL,
ALTER COLUMN "period" SET NOT NULL,
ALTER COLUMN "period" SET DEFAULT 0;
