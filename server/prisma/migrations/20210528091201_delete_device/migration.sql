/*
  Warnings:

  - You are about to drop the column `deviceId` on the `Activity` table. All the data in the column will be lost.
  - You are about to drop the `Device` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Device" DROP CONSTRAINT "Device_usersId_fkey";

-- DropForeignKey
ALTER TABLE "Activity" DROP CONSTRAINT "Activity_deviceId_fkey";

-- AlterTable
ALTER TABLE "Activity" DROP COLUMN "deviceId";

-- DropTable
DROP TABLE "Device";
