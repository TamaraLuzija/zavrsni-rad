/*
  Warnings:

  - The values [countActivity] on the enum `UserAchievementTypeType` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "UserAchievementTypeType_new" AS ENUM ('countWork', 'countBreak', 'timeWork', 'timeBreak', 'joinGroup', 'countCreateGroup', 'countExercises');
ALTER TABLE "UserAchievementType" ALTER COLUMN "type" TYPE "UserAchievementTypeType_new" USING ("type"::text::"UserAchievementTypeType_new");
ALTER TYPE "UserAchievementTypeType" RENAME TO "UserAchievementTypeType_old";
ALTER TYPE "UserAchievementTypeType_new" RENAME TO "UserAchievementTypeType";
DROP TYPE "UserAchievementTypeType_old";
COMMIT;
