/*
  Warnings:

  - You are about to drop the column `levelId` on the `User` table. All the data in the column will be lost.
  - You are about to drop the `Level` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "User" DROP CONSTRAINT "User_levelId_fkey";

-- AlterTable
ALTER TABLE "Activity" ADD COLUMN     "period" INTEGER;

-- AlterTable
ALTER TABLE "User" DROP COLUMN "levelId";

-- DropTable
DROP TABLE "Level";
