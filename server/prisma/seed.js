// @ts-check
// @ts-ignore
const db = new (require("@prisma/client").PrismaClient)();
const { hash } = require("bcryptjs");

const { createGroup, staticUserData, userAchievementTypes } = require("./static");

(async () => {
  await db.$connect();

  const uats = await db.$transaction(
    userAchievementTypes.map((data) => db.userAchievementType.create({ data: { ...data } }))
  );

  const PASS = await hash("tamtam123", 10);
  const u1 = await db.user.create({
    data: {
      username: "Foobar",
      email: "foo@bar.com",
      typeOfExercise: "walk",
      ...staticUserData(PASS, uats, [{ type: { connect: { id: uats[25].id } }, date: new Date() }]),
    },
  });

  const u2 = await db.user.create({
    data: {
      username: "TamTam",
      email: "tamara.luzija@fer.hr",
      typeOfExercise: "walk",
      ...staticUserData(PASS, uats),
    },
  });

  await db.group.create({ data: { name: "Tam", ...createGroup(u2, u1) } });
  await db.group.create({ data: { name: "FooGroup", ...createGroup(u1, u2) } });
  await db.group.create({ data: { name: "foo2", ...createGroup(u1, u2) } });

  await db.$disconnect();
})();
