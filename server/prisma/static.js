const { UserAchievementTypeType } = require("@prisma/client");
const UATT = UserAchievementTypeType;

const staticUserData = (hashedPassword, uats, other = []) => ({
  hashedPassword,
  userAchievement: {
    create: [
      // activities
      { type: { connect: { id: uats[0].id } }, date: new Date() }, // work 1
      { type: { connect: { id: uats[1].id } }, date: new Date() }, // work 2
      { type: { connect: { id: uats[4].id } }, date: new Date() }, // break 1

      // activities - time
      { type: { connect: { id: uats[12].id } }, date: new Date() }, // work 60
      { type: { connect: { id: uats[13].id } }, date: new Date() }, // work 240
      { type: { connect: { id: uats[16].id } }, date: new Date() }, // break 60

      // exercises
      { type: { connect: { id: uats[8].id } }, date: new Date() },
      { type: { connect: { id: uats[9].id } }, date: new Date() },

      // groups
      { type: { connect: { id: uats[24].id } }, date: new Date() },
      ...other,
    ],
  },
  timePreference: 120,
  breakLength: 30,
  activities: {
    create: [
      { date: new Date(), time: 120, fullTime: 120, complete: true, isBreak: false },
      { date: new Date(), time: 30, fullTime: 30, complete: true, isBreak: true },
      { date: new Date(), time: 120, fullTime: 120, complete: true, isBreak: false },
      { date: new Date(), time: 30, fullTime: 30, complete: true, isBreak: true },
      { date: new Date(), time: 45, fullTime: 120, complete: false, isBreak: false },
    ],
  },
  exercises: {
    create: [
      {
        name: "jumping jacks",
        description:
          "A jumping jack, also known as a star jump and called a side-straddle hop in the US military, is a physical jumping exercise performed by jumping to a position with the legs spread wide and the hands going overhead, sometimes in a clap, and then returning to a position with the feet together and the arms at the sides",
      },
      {
        name: "Burpees",
        description:
          "A burpee is essentially a two-part exercise: a pushup followed by a leap in the air",
      },
    ],
  },
});

const createGroup = (owner, user) => {
  return {
    owner: { connect: { id: owner.id } },
    users: { connect: { id: owner.id } },
    invitations: {
      create: {
        user: {
          connect: { id: user.id },
        },
      },
    },
  };
};

const userAchievementTypes = [
  { value: 1, type: UATT.countWork, name: "First one, only 10000000 to go" },
  { value: 2, type: UATT.countWork, name: "Second done" },
  { value: 10, type: UATT.countWork, name: "Nicely done, first 10 are always the hardest" },
  { value: 50, type: UATT.countWork, name: "Good job" },

  { value: 1, type: UATT.countBreak, name: "Take a break" },
  { value: 2, type: UATT.countBreak, name: "You deserved it" },
  { value: 10, type: UATT.countBreak, name: "You're on a role" },
  { value: 50, type: UATT.countBreak, name: "Slacking on the job :p" },

  { value: 1, type: UATT.countExercises, name: "First exercise" },
  { value: 2, type: UATT.countExercises, name: "Add some more" },
  { value: 5, type: UATT.countExercises, name: "Woo that's a lot" },
  { value: 10, type: UATT.countExercises, name: "ARE YOU CRAZY WITH THAT MANY EXERCISES" },

  { value: 60, type: UATT.timeWork, name: "First hour done, not just 1000 more" },
  { value: 240, type: UATT.timeWork, name: "Nicely done, now for the exercise" },
  { value: 1200, type: UATT.timeWork, name: "Woo hoo" },
  { value: 3840, type: UATT.timeWork, name: "You're on a role" },

  { value: 60, type: UATT.timeBreak, name: "First hour done, back to work" },
  { value: 240, type: UATT.timeBreak, name: "Woo you're getting good at this" },
  { value: 1200, type: UATT.timeBreak, name: "Congrats for being so active" },
  { value: 3840, type: UATT.timeBreak, name: "You're on a role" },

  { value: 1, type: UATT.joinGroup, name: "Welcome to the club" },
  { value: 2, type: UATT.joinGroup, name: "More social more better" },
  { value: 4, type: UATT.joinGroup, name: "So many groups so little time" },
  { value: 8, type: UATT.joinGroup, name: "Woooooooooooooo groups go brrrrrrrrr" },

  { value: 1, type: UATT.countCreateGroup, name: "Get the community started" },
  { value: 2, type: UATT.countCreateGroup, name: "Woo nice, multiple groups" },
  { value: 4, type: UATT.countCreateGroup, name: "You're on a role" },
  { value: 8, type: UATT.countCreateGroup, name: "Can you stop making new groups pls :)" },
];

module.exports = { staticUserData, createGroup, userAchievementTypes };
