import { app, nativeTheme, screen, Tray } from "electron";
import serve from "electron-serve";
import { createWindow } from "./helpers";
import { registerTimer } from "../main/timer";

export const isProd: boolean = process.env.NODE_ENV === "production";

if (isProd) {
  serve({ directory: "app" });
} else {
  app.setPath("userData", `${app.getPath("userData")} (development)`);
}

const windowOptions = { width: 600, height: 340, frame: false };
let tray: Tray | null = null;
let x = 0;

(async () => {
  await app.whenReady();
  const mainWindow = createWindow(windowOptions);

  // mainWindow.hide();

  const iconPath = `./resources/icons/dark.png`;
  tray = new Tray(iconPath);
  tray.setToolTip("This is my application.");
  tray.on("click", (event) => {
    const pos = screen.getCursorScreenPoint();
    const { height } = screen.getPrimaryDisplay().bounds;
    const y = pos.y < 50 ? 0 : height - windowOptions.height;

    if (x === 0) {
      // x = pos.x - 20;
      x = pos.x - windowOptions.width / 2;
    }

    mainWindow.setPosition(x, y);

    mainWindow.show();
    app.focus({ steal: true });
  });

  registerTimer();

  if (isProd) {
    await mainWindow.loadURL("app://./home.html");

    mainWindow.on("blur", () => {
      app.hide();
    });
  } else {
    const port = process.argv[2];
    await mainWindow.loadURL(`http://localhost:${port}/`);
    mainWindow.webContents.openDevTools();
  }
})();
