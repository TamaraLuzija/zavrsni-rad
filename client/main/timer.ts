import { ipcMain, Notification } from "electron";

let timePreference = undefined;
let breakPreference = undefined;

let time = 0;
let interval: NodeJS.Timeout;
let running = false;
let type: "work" | "break" = "work";
let exercises: string[] | null = null;

const notify = (data: Electron.NotificationConstructorOptions) =>
  new Promise((resolve, reject) => {
    const notification = new Notification(data);
    notification.show();

    notification.on("action", (event) => {
      resolve(true);
    });

    notification.on("close", (event) => {
      resolve(false);
    });

    setTimeout(() => {
      notification.removeAllListeners("close");
      notification.removeAllListeners("action");

      notification.close();
      resolve(null);
    }, 5000);
  });

const invertType = (type: "work" | "break") => (type === "work" ? "break" : "work");

export const registerTimer = () => {
  const tick = async () => {
    console.log(time);

    if (time === -1) {
      running = false;

      let randomEx = null;
      if (exercises) {
        randomEx = exercises[Math.floor(Math.random() * exercises.length)];
      }

      const res = await notify({
        actions: [{ type: "button", text: "Ok" }],
        timeoutType: "never",
        urgency: "critical",
        ...(type === "work"
          ? {
              title: `Work session done`,
              body: `Now "${randomEx || "walk"}" for ${
                breakPreference > 60 * 60
                  ? `${breakPreference / 60 / 60}h and ${(breakPreference / 60) % 60}min`
                  : `${breakPreference / 60}min`
              }`,
            }
          : {
              title: `Break session done`,
              body: "Back to work",
            }),
      });

      if (res === true) {
        running = true;
      }

      type = invertType(type);
      time = type === "work" ? timePreference : breakPreference;

      return {
        running,
        time,
        type,
        event: {
          type: "change",
          data: {
            fullTime: type === "work" ? timePreference : breakPreference,
            value: 0,
            type: invertType(type),
          },
        },
      };
    }

    let event = null;
    if (time % 30 === 0) {
      event = {
        type: time === 0 ? "finalPing" : "ping",
        data: {
          fullTime: type === "work" ? timePreference : breakPreference,
          value: (type === "work" ? timePreference : breakPreference) - time,
          type,
        },
      };
    }

    return {
      running,
      time: time--,
      type,
      event,
    };
  };

  ipcMain.on("setExercises", (event, data?: string[]) => {
    exercises = data || null;
  });

  ipcMain.on("startTimer", (event, arg) => {
    if (!running) {
      running = true;
    }

    event.reply("state", running);

    if (interval === undefined) {
      // tick().then((res) => {
      //   event.reply("tick", res);
      //   event.reply("state", running);
      // });

      interval = setInterval(() => {
        if (running) {
          tick().then((res) => {
            event.reply("tick", res);
            event.reply("state", running);
          });
        }
      }, 50);
    }
  });

  ipcMain.on("resetTimer", (event) => {
    time = 0;
    event.reply("tick", { time, type });
  });

  ipcMain.on("stopTimer", (event) => {
    running = false;
    event.reply("state", running);
    clearInterval(interval);
    interval = undefined;
  });

  ipcMain.on("getTimer", (event) => {
    event.reply("state", running);
    event.reply("tick", { time, type });
  });

  ipcMain.on("setTimePreference", (event, data) => {
    console.log("update time");

    type = "work";
    time = data.work;
    event.reply("tick", { time, type });

    timePreference = data.work;
    breakPreference = data.break;
  });
};
