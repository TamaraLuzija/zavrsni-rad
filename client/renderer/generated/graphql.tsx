import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
};

export type Activity = {
  __typename?: "Activity";
  id: Scalars["Int"];
  date: Scalars["DateTime"];
  time: Scalars["Int"];
  fullTime: Scalars["Int"];
  complete: Scalars["Boolean"];
  isBreak: Scalars["Boolean"];
  exercisesId?: Maybe<Scalars["Int"]>;
  usersId: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  exercise?: Maybe<Exercise>;
};

export type AuthenticateType = {
  __typename?: "AuthenticateType";
  token: Scalars["String"];
  user: User;
};

export type CompletionItem = {
  __typename?: "CompletionItem";
  type: UserAchievementTypeType;
  value: Scalars["Int"];
};

export type DataResponse = {
  __typename?: "DataResponse";
  id: Scalars["String"];
  name: Scalars["String"];
  maxLevel: Scalars["Int"];
  userLevel?: Maybe<Scalars["Int"]>;
  currentValue?: Maybe<Scalars["Int"]>;
  nextValue?: Maybe<Scalars["Int"]>;
  message?: Maybe<Scalars["String"]>;
};

export type Exercise = {
  __typename?: "Exercise";
  id: Scalars["Int"];
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  link?: Maybe<Scalars["String"]>;
  userId: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
};

export type Group = {
  __typename?: "Group";
  id: Scalars["Int"];
  name: Scalars["String"];
  imageUrl?: Maybe<Scalars["String"]>;
  inviteLink?: Maybe<Scalars["String"]>;
  public: Scalars["Boolean"];
  ownerId: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  usersWithRank?: Maybe<Array<UserWithRank>>;
  invitations: Array<Invitation>;
  owner: User;
};

export type GroupInvitationsArgs = {
  joined?: Maybe<Scalars["Boolean"]>;
};

export type GroupInput = {
  name: Scalars["String"];
  memberIds: Array<Scalars["Int"]>;
};

export type Invitation = {
  __typename?: "Invitation";
  id: Scalars["Int"];
  groupId: Scalars["Int"];
  userId: Scalars["Int"];
  joinedAt?: Maybe<Scalars["DateTime"]>;
  createdAt: Scalars["DateTime"];
  user?: Maybe<User>;
  group?: Maybe<Group>;
};

export type Mutation = {
  __typename?: "Mutation";
  signUp: AuthenticateType;
  signIn: AuthenticateType;
  respondToInvite: Scalars["Boolean"];
  createGroup: Group;
  kickUser: Group;
  leave: User;
  inviteToGroup: Group;
  deleteInvite: Group;
  updateTime: User;
  updateQuestions: User;
  createExercise: Exercise;
  updateExercise: Exercise;
  deleteExercise: Exercise;
  createActivity: Activity;
  updateActivity: Activity;
};

export type MutationSignUpArgs = {
  data: SignUpInput;
};

export type MutationSignInArgs = {
  password: Scalars["String"];
  email: Scalars["String"];
};

export type MutationRespondToInviteArgs = {
  accept: Scalars["Boolean"];
  inviteId: Scalars["Int"];
};

export type MutationCreateGroupArgs = {
  data: GroupInput;
};

export type MutationKickUserArgs = {
  userId: Scalars["Int"];
  groupId: Scalars["Int"];
};

export type MutationLeaveArgs = {
  groupId: Scalars["Int"];
};

export type MutationInviteToGroupArgs = {
  userIds: Array<Scalars["Int"]>;
  id: Scalars["Int"];
};

export type MutationDeleteInviteArgs = {
  inviteId: Scalars["Int"];
  groupId: Scalars["Int"];
};

export type MutationUpdateTimeArgs = {
  data: TimePreferenceInputs;
};

export type MutationUpdateQuestionsArgs = {
  data: QuestionsInputs;
};

export type MutationCreateExerciseArgs = {
  data: NewExInput;
};

export type MutationUpdateExerciseArgs = {
  data: UpdateExInput;
  id: Scalars["Int"];
};

export type MutationDeleteExerciseArgs = {
  id: Scalars["Int"];
};

export type MutationCreateActivityArgs = {
  fullTime: Scalars["Int"];
  isBreak: Scalars["Boolean"];
};

export type MutationUpdateActivityArgs = {
  data: UpdateActivityType;
  id: Scalars["Int"];
};

export type NewExInput = {
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  link?: Maybe<Scalars["String"]>;
};

export type ProfileStats = {
  __typename?: "ProfileStats";
  work: Stat;
  break: Stat;
  diff: Scalars["Int"];
  diffBefore: Scalars["Int"];
};

export type Query = {
  __typename?: "Query";
  me?: Maybe<User>;
  profileStats: ProfileStats;
  groups: Array<Group>;
  users?: Maybe<Array<User>>;
  userAchievements?: Maybe<Array<DataResponse>>;
  userAchievementType?: Maybe<UserAchievementsQueryResponse>;
  activities: Array<Activity>;
};

export type QuestionsInputs = {
  timePreference?: Maybe<Scalars["Int"]>;
  breakLength?: Maybe<Scalars["Int"]>;
  typeOfExercise?: Maybe<Scalars["String"]>;
};

export type SignUpInput = {
  email: Scalars["String"];
  username: Scalars["String"];
  password: Scalars["String"];
};

export type Stat = {
  __typename?: "Stat";
  now: Scalars["Int"];
  before: Scalars["Int"];
  countNow?: Maybe<Scalars["Int"]>;
  countBefore?: Maybe<Scalars["Int"]>;
};

export type Stats = {
  __typename?: "Stats";
  work: Scalars["Int"];
  break: Scalars["Int"];
};

export type TimePreferenceInputs = {
  timePreference?: Maybe<Scalars["Int"]>;
  breakLength?: Maybe<Scalars["Int"]>;
};

export type UpdateActivityType = {
  isBreak?: Maybe<Scalars["Boolean"]>;
  isComplete?: Maybe<Scalars["Boolean"]>;
  exerciseId?: Maybe<Scalars["Int"]>;
  time?: Maybe<Scalars["Int"]>;
  fullTime?: Maybe<Scalars["Int"]>;
};

export type UpdateExInput = {
  name?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  link?: Maybe<Scalars["String"]>;
};

export type User = {
  __typename?: "User";
  id: Scalars["Int"];
  email: Scalars["String"];
  username?: Maybe<Scalars["String"]>;
  hashedPassword: Scalars["String"];
  workHours?: Maybe<Scalars["Int"]>;
  workplace?: Maybe<Scalars["String"]>;
  typeOfExercise?: Maybe<Scalars["String"]>;
  timePreference: Scalars["Int"];
  breakLength: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  groups: Array<Group>;
  stats: Stats;
  invitations: Array<Invitation>;
  exercises: Array<Exercise>;
};

export type UserInvitationsArgs = {
  joined?: Maybe<Scalars["Boolean"]>;
};

export type UserAchievementType = {
  __typename?: "UserAchievementType";
  id: Scalars["Int"];
  name: Scalars["String"];
  type: UserAchievementTypeType;
  value: Scalars["Int"];
  color?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  imageUrl?: Maybe<Scalars["String"]>;
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  typeName: Scalars["String"];
};

export enum UserAchievementTypeType {
  CountWork = "countWork",
  CountBreak = "countBreak",
  TimeWork = "timeWork",
  TimeBreak = "timeBreak",
  JoinGroup = "joinGroup",
  CountCreateGroup = "countCreateGroup",
  CountExercises = "countExercises",
}

export type UserAchievementsQueryResponse = {
  __typename?: "UserAchievementsQueryResponse";
  types: Array<UserAchievementType>;
  userCompletedTypes: Array<CompletionItem>;
  allTypes: Array<CompletionItem>;
};

export type UserWithRank = {
  __typename?: "UserWithRank";
  id: Scalars["Int"];
  email: Scalars["String"];
  username?: Maybe<Scalars["String"]>;
  hashedPassword: Scalars["String"];
  workHours?: Maybe<Scalars["Int"]>;
  workplace?: Maybe<Scalars["String"]>;
  typeOfExercise?: Maybe<Scalars["String"]>;
  timePreference: Scalars["Int"];
  breakLength: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  groups: Array<Group>;
  stats: Stats;
  invitations: Array<Invitation>;
  exercises: Array<Exercise>;
  rank: Scalars["Int"];
  groupId: Scalars["Int"];
};

export type UserWithRankInvitationsArgs = {
  joined?: Maybe<Scalars["Boolean"]>;
};

export type SignInMutationVariables = Exact<{
  email: Scalars["String"];
  password: Scalars["String"];
}>;

export type SignInMutation = { __typename?: "Mutation" } & {
  signIn: { __typename?: "AuthenticateType" } & Pick<AuthenticateType, "token"> & {
      user: { __typename?: "User" } & Pick<User, "id" | "username">;
    };
};

export type SignUpMutationVariables = Exact<{
  email: Scalars["String"];
  username: Scalars["String"];
  password: Scalars["String"];
}>;

export type SignUpMutation = { __typename?: "Mutation" } & {
  signUp: { __typename?: "AuthenticateType" } & Pick<AuthenticateType, "token"> & {
      user: { __typename?: "User" } & Pick<User, "id" | "username">;
    };
};

export type MeQueryVariables = Exact<{ [key: string]: never }>;

export type MeQuery = { __typename?: "Query" } & {
  me?: Maybe<
    { __typename?: "User" } & Pick<
      User,
      | "id"
      | "email"
      | "username"
      | "workHours"
      | "workplace"
      | "typeOfExercise"
      | "timePreference"
      | "breakLength"
    > & {
        exercises: Array<
          { __typename?: "Exercise" } & Pick<Exercise, "id" | "name" | "description" | "link">
        >;
        invitations: Array<
          { __typename?: "Invitation" } & Pick<Invitation, "id"> & {
              group?: Maybe<
                { __typename?: "Group" } & Pick<Group, "id" | "name"> & {
                    owner: { __typename?: "User" } & Pick<User, "id" | "username">;
                    usersWithRank?: Maybe<
                      Array<
                        { __typename?: "UserWithRank" } & Pick<
                          UserWithRank,
                          "id" | "groupId" | "username"
                        >
                      >
                    >;
                  }
              >;
            }
        >;
        groups: Array<
          { __typename?: "Group" } & Pick<Group, "id" | "name" | "ownerId"> & {
              usersWithRank?: Maybe<
                Array<
                  { __typename?: "UserWithRank" } & Pick<
                    UserWithRank,
                    "id" | "groupId" | "rank" | "username"
                  > & { stats: { __typename?: "Stats" } & Pick<Stats, "work" | "break"> }
                >
              >;
              invitations: Array<
                { __typename?: "Invitation" } & Pick<Invitation, "id"> & {
                    user?: Maybe<{ __typename?: "User" } & Pick<User, "id" | "username">>;
                  }
              >;
            }
        >;
      }
  >;
};

export type CreateExerciseMutationVariables = Exact<{
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  link?: Maybe<Scalars["String"]>;
}>;

export type CreateExerciseMutation = { __typename?: "Mutation" } & {
  createExercise: { __typename?: "Exercise" } & Pick<
    Exercise,
    "id" | "name" | "description" | "link"
  >;
};

export type UpdateExerciseMutationVariables = Exact<{
  id: Scalars["Int"];
  name?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  link?: Maybe<Scalars["String"]>;
}>;

export type UpdateExerciseMutation = { __typename?: "Mutation" } & {
  updateExercise: { __typename?: "Exercise" } & Pick<
    Exercise,
    "id" | "name" | "description" | "link"
  >;
};

export type DeleteExerciseMutationVariables = Exact<{
  id: Scalars["Int"];
}>;

export type DeleteExerciseMutation = { __typename?: "Mutation" } & {
  deleteExercise: { __typename?: "Exercise" } & Pick<Exercise, "id" | "name">;
};

export type CreateActivityMutationVariables = Exact<{
  isBreak: Scalars["Boolean"];
  fullTime: Scalars["Int"];
}>;

export type CreateActivityMutation = { __typename?: "Mutation" } & {
  createActivity: { __typename?: "Activity" } & Pick<
    Activity,
    "id" | "complete" | "isBreak" | "time"
  >;
};

export type UpdateActivityMutationVariables = Exact<{
  id: Scalars["Int"];
  time?: Maybe<Scalars["Int"]>;
  isComplete?: Maybe<Scalars["Boolean"]>;
  fullTime?: Maybe<Scalars["Int"]>;
}>;

export type UpdateActivityMutation = { __typename?: "Mutation" } & {
  updateActivity: { __typename?: "Activity" } & Pick<
    Activity,
    "id" | "complete" | "isBreak" | "time"
  >;
};

export type ActivitiesQueryVariables = Exact<{ [key: string]: never }>;

export type ActivitiesQuery = { __typename?: "Query" } & {
  activities: Array<
    { __typename?: "Activity" } & Pick<
      Activity,
      "id" | "complete" | "isBreak" | "time" | "fullTime" | "date"
    > & { exercise?: Maybe<{ __typename?: "Exercise" } & Pick<Exercise, "id" | "name">> }
  >;
};

export type GoalsQueryVariables = Exact<{ [key: string]: never }>;

export type GoalsQuery = { __typename?: "Query" } & {
  userAchievementType?: Maybe<
    { __typename?: "UserAchievementsQueryResponse" } & {
      types: Array<
        { __typename?: "UserAchievementType" } & Pick<
          UserAchievementType,
          "id" | "name" | "value" | "type" | "typeName"
        >
      >;
      userCompletedTypes: Array<
        { __typename?: "CompletionItem" } & Pick<CompletionItem, "type" | "value">
      >;
      allTypes: Array<{ __typename?: "CompletionItem" } & Pick<CompletionItem, "type" | "value">>;
    }
  >;
};

export type UserAchievementsQueryVariables = Exact<{ [key: string]: never }>;

export type UserAchievementsQuery = { __typename?: "Query" } & {
  userAchievements?: Maybe<
    Array<
      { __typename?: "DataResponse" } & Pick<
        DataResponse,
        "id" | "name" | "maxLevel" | "userLevel" | "message" | "currentValue" | "nextValue"
      >
    >
  >;
};

export type CreateGroupMutationVariables = Exact<{
  name: Scalars["String"];
  membersIds: Array<Scalars["Int"]> | Scalars["Int"];
}>;

export type CreateGroupMutation = { __typename?: "Mutation" } & {
  createGroup: { __typename?: "Group" } & Pick<Group, "id" | "name"> & {
      usersWithRank?: Maybe<
        Array<{ __typename?: "UserWithRank" } & Pick<UserWithRank, "id" | "groupId" | "username">>
      >;
      invitations: Array<
        { __typename?: "Invitation" } & Pick<Invitation, "id"> & {
            user?: Maybe<{ __typename?: "User" } & Pick<User, "id" | "username">>;
          }
      >;
    };
};

export type RespondToInviteMutationVariables = Exact<{
  inviteId: Scalars["Int"];
  accept: Scalars["Boolean"];
}>;

export type RespondToInviteMutation = { __typename?: "Mutation" } & Pick<
  Mutation,
  "respondToInvite"
>;

export type InviteToGroupMutationVariables = Exact<{
  groupId: Scalars["Int"];
  users: Array<Scalars["Int"]> | Scalars["Int"];
}>;

export type InviteToGroupMutation = { __typename?: "Mutation" } & {
  inviteToGroup: { __typename?: "Group" } & Pick<Group, "id"> & {
      invitations: Array<
        { __typename?: "Invitation" } & Pick<Invitation, "id"> & {
            user?: Maybe<{ __typename?: "User" } & Pick<User, "id" | "username">>;
          }
      >;
    };
};

export type KickUserMutationVariables = Exact<{
  groupId: Scalars["Int"];
  userId: Scalars["Int"];
}>;

export type KickUserMutation = { __typename?: "Mutation" } & {
  kickUser: { __typename?: "Group" } & Pick<Group, "id"> & {
      usersWithRank?: Maybe<
        Array<{ __typename?: "UserWithRank" } & Pick<UserWithRank, "id" | "groupId" | "username">>
      >;
    };
};

export type LeaveGroupMutationVariables = Exact<{
  groupId: Scalars["Int"];
}>;

export type LeaveGroupMutation = { __typename?: "Mutation" } & {
  leave: { __typename?: "User" } & Pick<User, "id"> & {
      groups: Array<
        { __typename?: "Group" } & Pick<Group, "id" | "name" | "ownerId"> & {
            usersWithRank?: Maybe<
              Array<
                { __typename?: "UserWithRank" } & Pick<
                  UserWithRank,
                  "id" | "groupId" | "rank" | "username"
                > & { stats: { __typename?: "Stats" } & Pick<Stats, "work" | "break"> }
              >
            >;
            invitations: Array<
              { __typename?: "Invitation" } & Pick<Invitation, "id"> & {
                  user?: Maybe<{ __typename?: "User" } & Pick<User, "id" | "username">>;
                }
            >;
          }
      >;
    };
};

export type DeleteInviteMutationVariables = Exact<{
  groupId: Scalars["Int"];
  inviteId: Scalars["Int"];
}>;

export type DeleteInviteMutation = { __typename?: "Mutation" } & {
  deleteInvite: { __typename?: "Group" } & Pick<Group, "id"> & {
      invitations: Array<{ __typename?: "Invitation" } & Pick<Invitation, "id" | "groupId">>;
    };
};

export type QuestionsMutationVariables = Exact<{
  timePreference?: Maybe<Scalars["Int"]>;
  breakLength?: Maybe<Scalars["Int"]>;
  typeOfExercise?: Maybe<Scalars["String"]>;
}>;

export type QuestionsMutation = { __typename?: "Mutation" } & {
  updateQuestions: { __typename?: "User" } & Pick<
    User,
    "id" | "timePreference" | "breakLength" | "typeOfExercise"
  >;
};

export type UpdateTimePreferenceMutationVariables = Exact<{
  time?: Maybe<Scalars["Int"]>;
  breakLength?: Maybe<Scalars["Int"]>;
  type?: Maybe<Scalars["String"]>;
}>;

export type UpdateTimePreferenceMutation = { __typename?: "Mutation" } & {
  updateTime: { __typename?: "User" } & Pick<User, "id" | "timePreference" | "breakLength">;
  updateQuestions: { __typename?: "User" } & Pick<User, "id" | "typeOfExercise">;
};

export type UsersQueryVariables = Exact<{ [key: string]: never }>;

export type UsersQuery = { __typename?: "Query" } & {
  users?: Maybe<Array<{ __typename?: "User" } & Pick<User, "id" | "username">>>;
};

export type StatsQueryVariables = Exact<{ [key: string]: never }>;

export type StatsQuery = { __typename?: "Query" } & {
  profileStats: { __typename?: "ProfileStats" } & Pick<ProfileStats, "diff" | "diffBefore"> & {
      work: { __typename?: "Stat" } & Pick<Stat, "now" | "before" | "countNow" | "countBefore">;
      break: { __typename?: "Stat" } & Pick<Stat, "now" | "before" | "countNow" | "countBefore">;
    };
};

export const SignInDocument = gql`
  mutation signIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
      user {
        id
        username
      }
    }
  }
`;
export type SignInMutationFn = Apollo.MutationFunction<SignInMutation, SignInMutationVariables>;

/**
 * __useSignInMutation__
 *
 * To run a mutation, you first call `useSignInMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignInMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signInMutation, { data, loading, error }] = useSignInMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignInMutation(
  baseOptions?: Apollo.MutationHookOptions<SignInMutation, SignInMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<SignInMutation, SignInMutationVariables>(SignInDocument, options);
}
export type SignInMutationHookResult = ReturnType<typeof useSignInMutation>;
export type SignInMutationResult = Apollo.MutationResult<SignInMutation>;
export type SignInMutationOptions = Apollo.BaseMutationOptions<
  SignInMutation,
  SignInMutationVariables
>;
export const SignUpDocument = gql`
  mutation signUp($email: String!, $username: String!, $password: String!) {
    signUp(data: { email: $email, username: $username, password: $password }) {
      token
      user {
        id
        username
      }
    }
  }
`;
export type SignUpMutationFn = Apollo.MutationFunction<SignUpMutation, SignUpMutationVariables>;

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignUpMutation(
  baseOptions?: Apollo.MutationHookOptions<SignUpMutation, SignUpMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, options);
}
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = Apollo.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = Apollo.BaseMutationOptions<
  SignUpMutation,
  SignUpMutationVariables
>;
export const MeDocument = gql`
  query ME {
    me {
      id
      email
      username
      workHours
      workplace
      typeOfExercise
      timePreference
      breakLength
      exercises {
        id
        name
        description
        link
      }
      invitations(joined: false) {
        id
        group {
          id
          name
          owner {
            id
            username
          }
          usersWithRank {
            id
            groupId
            username
          }
        }
      }
      groups {
        id
        name
        ownerId
        usersWithRank {
          id
          groupId
          rank
          username
          stats {
            work
            break
          }
        }
        invitations(joined: false) {
          id
          user {
            id
            username
          }
        }
      }
    }
  }
`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export function useMeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const CreateExerciseDocument = gql`
  mutation createExercise($name: String!, $description: String, $link: String) {
    createExercise(data: { name: $name, description: $description, link: $link }) {
      id
      name
      description
      link
    }
  }
`;
export type CreateExerciseMutationFn = Apollo.MutationFunction<
  CreateExerciseMutation,
  CreateExerciseMutationVariables
>;

/**
 * __useCreateExerciseMutation__
 *
 * To run a mutation, you first call `useCreateExerciseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateExerciseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createExerciseMutation, { data, loading, error }] = useCreateExerciseMutation({
 *   variables: {
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      link: // value for 'link'
 *   },
 * });
 */
export function useCreateExerciseMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateExerciseMutation, CreateExerciseMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateExerciseMutation, CreateExerciseMutationVariables>(
    CreateExerciseDocument,
    options
  );
}
export type CreateExerciseMutationHookResult = ReturnType<typeof useCreateExerciseMutation>;
export type CreateExerciseMutationResult = Apollo.MutationResult<CreateExerciseMutation>;
export type CreateExerciseMutationOptions = Apollo.BaseMutationOptions<
  CreateExerciseMutation,
  CreateExerciseMutationVariables
>;
export const UpdateExerciseDocument = gql`
  mutation updateExercise($id: Int!, $name: String, $description: String, $link: String) {
    updateExercise(id: $id, data: { name: $name, description: $description, link: $link }) {
      id
      name
      description
      link
    }
  }
`;
export type UpdateExerciseMutationFn = Apollo.MutationFunction<
  UpdateExerciseMutation,
  UpdateExerciseMutationVariables
>;

/**
 * __useUpdateExerciseMutation__
 *
 * To run a mutation, you first call `useUpdateExerciseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateExerciseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateExerciseMutation, { data, loading, error }] = useUpdateExerciseMutation({
 *   variables: {
 *      id: // value for 'id'
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      link: // value for 'link'
 *   },
 * });
 */
export function useUpdateExerciseMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateExerciseMutation, UpdateExerciseMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateExerciseMutation, UpdateExerciseMutationVariables>(
    UpdateExerciseDocument,
    options
  );
}
export type UpdateExerciseMutationHookResult = ReturnType<typeof useUpdateExerciseMutation>;
export type UpdateExerciseMutationResult = Apollo.MutationResult<UpdateExerciseMutation>;
export type UpdateExerciseMutationOptions = Apollo.BaseMutationOptions<
  UpdateExerciseMutation,
  UpdateExerciseMutationVariables
>;
export const DeleteExerciseDocument = gql`
  mutation deleteExercise($id: Int!) {
    deleteExercise(id: $id) {
      id
      name
    }
  }
`;
export type DeleteExerciseMutationFn = Apollo.MutationFunction<
  DeleteExerciseMutation,
  DeleteExerciseMutationVariables
>;

/**
 * __useDeleteExerciseMutation__
 *
 * To run a mutation, you first call `useDeleteExerciseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteExerciseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteExerciseMutation, { data, loading, error }] = useDeleteExerciseMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteExerciseMutation(
  baseOptions?: Apollo.MutationHookOptions<DeleteExerciseMutation, DeleteExerciseMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<DeleteExerciseMutation, DeleteExerciseMutationVariables>(
    DeleteExerciseDocument,
    options
  );
}
export type DeleteExerciseMutationHookResult = ReturnType<typeof useDeleteExerciseMutation>;
export type DeleteExerciseMutationResult = Apollo.MutationResult<DeleteExerciseMutation>;
export type DeleteExerciseMutationOptions = Apollo.BaseMutationOptions<
  DeleteExerciseMutation,
  DeleteExerciseMutationVariables
>;
export const CreateActivityDocument = gql`
  mutation createActivity($isBreak: Boolean!, $fullTime: Int!) {
    createActivity(isBreak: $isBreak, fullTime: $fullTime) {
      id
      complete
      isBreak
      time
    }
  }
`;
export type CreateActivityMutationFn = Apollo.MutationFunction<
  CreateActivityMutation,
  CreateActivityMutationVariables
>;

/**
 * __useCreateActivityMutation__
 *
 * To run a mutation, you first call `useCreateActivityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateActivityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createActivityMutation, { data, loading, error }] = useCreateActivityMutation({
 *   variables: {
 *      isBreak: // value for 'isBreak'
 *      fullTime: // value for 'fullTime'
 *   },
 * });
 */
export function useCreateActivityMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateActivityMutation, CreateActivityMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateActivityMutation, CreateActivityMutationVariables>(
    CreateActivityDocument,
    options
  );
}
export type CreateActivityMutationHookResult = ReturnType<typeof useCreateActivityMutation>;
export type CreateActivityMutationResult = Apollo.MutationResult<CreateActivityMutation>;
export type CreateActivityMutationOptions = Apollo.BaseMutationOptions<
  CreateActivityMutation,
  CreateActivityMutationVariables
>;
export const UpdateActivityDocument = gql`
  mutation updateActivity($id: Int!, $time: Int, $isComplete: Boolean, $fullTime: Int) {
    updateActivity(id: $id, data: { time: $time, isComplete: $isComplete, fullTime: $fullTime }) {
      id
      complete
      isBreak
      time
    }
  }
`;
export type UpdateActivityMutationFn = Apollo.MutationFunction<
  UpdateActivityMutation,
  UpdateActivityMutationVariables
>;

/**
 * __useUpdateActivityMutation__
 *
 * To run a mutation, you first call `useUpdateActivityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateActivityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateActivityMutation, { data, loading, error }] = useUpdateActivityMutation({
 *   variables: {
 *      id: // value for 'id'
 *      time: // value for 'time'
 *      isComplete: // value for 'isComplete'
 *      fullTime: // value for 'fullTime'
 *   },
 * });
 */
export function useUpdateActivityMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateActivityMutation, UpdateActivityMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateActivityMutation, UpdateActivityMutationVariables>(
    UpdateActivityDocument,
    options
  );
}
export type UpdateActivityMutationHookResult = ReturnType<typeof useUpdateActivityMutation>;
export type UpdateActivityMutationResult = Apollo.MutationResult<UpdateActivityMutation>;
export type UpdateActivityMutationOptions = Apollo.BaseMutationOptions<
  UpdateActivityMutation,
  UpdateActivityMutationVariables
>;
export const ActivitiesDocument = gql`
  query Activities {
    activities {
      id
      complete
      isBreak
      time
      fullTime
      date
      exercise {
        id
        name
      }
    }
  }
`;

/**
 * __useActivitiesQuery__
 *
 * To run a query within a React component, call `useActivitiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useActivitiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useActivitiesQuery({
 *   variables: {
 *   },
 * });
 */
export function useActivitiesQuery(
  baseOptions?: Apollo.QueryHookOptions<ActivitiesQuery, ActivitiesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<ActivitiesQuery, ActivitiesQueryVariables>(ActivitiesDocument, options);
}
export function useActivitiesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<ActivitiesQuery, ActivitiesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<ActivitiesQuery, ActivitiesQueryVariables>(
    ActivitiesDocument,
    options
  );
}
export type ActivitiesQueryHookResult = ReturnType<typeof useActivitiesQuery>;
export type ActivitiesLazyQueryHookResult = ReturnType<typeof useActivitiesLazyQuery>;
export type ActivitiesQueryResult = Apollo.QueryResult<ActivitiesQuery, ActivitiesQueryVariables>;
export const GoalsDocument = gql`
  query GOALS {
    userAchievementType {
      types {
        id
        name
        value
        type
        typeName
      }
      userCompletedTypes {
        type
        value
      }
      allTypes {
        type
        value
      }
    }
  }
`;

/**
 * __useGoalsQuery__
 *
 * To run a query within a React component, call `useGoalsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGoalsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGoalsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGoalsQuery(
  baseOptions?: Apollo.QueryHookOptions<GoalsQuery, GoalsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GoalsQuery, GoalsQueryVariables>(GoalsDocument, options);
}
export function useGoalsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GoalsQuery, GoalsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GoalsQuery, GoalsQueryVariables>(GoalsDocument, options);
}
export type GoalsQueryHookResult = ReturnType<typeof useGoalsQuery>;
export type GoalsLazyQueryHookResult = ReturnType<typeof useGoalsLazyQuery>;
export type GoalsQueryResult = Apollo.QueryResult<GoalsQuery, GoalsQueryVariables>;
export const UserAchievementsDocument = gql`
  query UserAchievements {
    userAchievements {
      id
      name
      maxLevel
      userLevel
      message
      currentValue
      nextValue
    }
  }
`;

/**
 * __useUserAchievementsQuery__
 *
 * To run a query within a React component, call `useUserAchievementsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserAchievementsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserAchievementsQuery({
 *   variables: {
 *   },
 * });
 */
export function useUserAchievementsQuery(
  baseOptions?: Apollo.QueryHookOptions<UserAchievementsQuery, UserAchievementsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<UserAchievementsQuery, UserAchievementsQueryVariables>(
    UserAchievementsDocument,
    options
  );
}
export function useUserAchievementsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<UserAchievementsQuery, UserAchievementsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<UserAchievementsQuery, UserAchievementsQueryVariables>(
    UserAchievementsDocument,
    options
  );
}
export type UserAchievementsQueryHookResult = ReturnType<typeof useUserAchievementsQuery>;
export type UserAchievementsLazyQueryHookResult = ReturnType<typeof useUserAchievementsLazyQuery>;
export type UserAchievementsQueryResult = Apollo.QueryResult<
  UserAchievementsQuery,
  UserAchievementsQueryVariables
>;
export const CreateGroupDocument = gql`
  mutation createGroup($name: String!, $membersIds: [Int!]!) {
    createGroup(data: { name: $name, memberIds: $membersIds }) {
      id
      name
      usersWithRank {
        id
        groupId
        username
      }
      invitations {
        id
        user {
          id
          username
        }
      }
    }
  }
`;
export type CreateGroupMutationFn = Apollo.MutationFunction<
  CreateGroupMutation,
  CreateGroupMutationVariables
>;

/**
 * __useCreateGroupMutation__
 *
 * To run a mutation, you first call `useCreateGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createGroupMutation, { data, loading, error }] = useCreateGroupMutation({
 *   variables: {
 *      name: // value for 'name'
 *      membersIds: // value for 'membersIds'
 *   },
 * });
 */
export function useCreateGroupMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateGroupMutation, CreateGroupMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateGroupMutation, CreateGroupMutationVariables>(
    CreateGroupDocument,
    options
  );
}
export type CreateGroupMutationHookResult = ReturnType<typeof useCreateGroupMutation>;
export type CreateGroupMutationResult = Apollo.MutationResult<CreateGroupMutation>;
export type CreateGroupMutationOptions = Apollo.BaseMutationOptions<
  CreateGroupMutation,
  CreateGroupMutationVariables
>;
export const RespondToInviteDocument = gql`
  mutation respondToInvite($inviteId: Int!, $accept: Boolean!) {
    respondToInvite(inviteId: $inviteId, accept: $accept)
  }
`;
export type RespondToInviteMutationFn = Apollo.MutationFunction<
  RespondToInviteMutation,
  RespondToInviteMutationVariables
>;

/**
 * __useRespondToInviteMutation__
 *
 * To run a mutation, you first call `useRespondToInviteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRespondToInviteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [respondToInviteMutation, { data, loading, error }] = useRespondToInviteMutation({
 *   variables: {
 *      inviteId: // value for 'inviteId'
 *      accept: // value for 'accept'
 *   },
 * });
 */
export function useRespondToInviteMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RespondToInviteMutation,
    RespondToInviteMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<RespondToInviteMutation, RespondToInviteMutationVariables>(
    RespondToInviteDocument,
    options
  );
}
export type RespondToInviteMutationHookResult = ReturnType<typeof useRespondToInviteMutation>;
export type RespondToInviteMutationResult = Apollo.MutationResult<RespondToInviteMutation>;
export type RespondToInviteMutationOptions = Apollo.BaseMutationOptions<
  RespondToInviteMutation,
  RespondToInviteMutationVariables
>;
export const InviteToGroupDocument = gql`
  mutation inviteToGroup($groupId: Int!, $users: [Int!]!) {
    inviteToGroup(id: $groupId, userIds: $users) {
      id
      invitations {
        id
        user {
          id
          username
        }
      }
    }
  }
`;
export type InviteToGroupMutationFn = Apollo.MutationFunction<
  InviteToGroupMutation,
  InviteToGroupMutationVariables
>;

/**
 * __useInviteToGroupMutation__
 *
 * To run a mutation, you first call `useInviteToGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInviteToGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [inviteToGroupMutation, { data, loading, error }] = useInviteToGroupMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *      users: // value for 'users'
 *   },
 * });
 */
export function useInviteToGroupMutation(
  baseOptions?: Apollo.MutationHookOptions<InviteToGroupMutation, InviteToGroupMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<InviteToGroupMutation, InviteToGroupMutationVariables>(
    InviteToGroupDocument,
    options
  );
}
export type InviteToGroupMutationHookResult = ReturnType<typeof useInviteToGroupMutation>;
export type InviteToGroupMutationResult = Apollo.MutationResult<InviteToGroupMutation>;
export type InviteToGroupMutationOptions = Apollo.BaseMutationOptions<
  InviteToGroupMutation,
  InviteToGroupMutationVariables
>;
export const KickUserDocument = gql`
  mutation kickUser($groupId: Int!, $userId: Int!) {
    kickUser(groupId: $groupId, userId: $userId) {
      id
      usersWithRank {
        id
        groupId
        username
      }
    }
  }
`;
export type KickUserMutationFn = Apollo.MutationFunction<
  KickUserMutation,
  KickUserMutationVariables
>;

/**
 * __useKickUserMutation__
 *
 * To run a mutation, you first call `useKickUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useKickUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [kickUserMutation, { data, loading, error }] = useKickUserMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useKickUserMutation(
  baseOptions?: Apollo.MutationHookOptions<KickUserMutation, KickUserMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<KickUserMutation, KickUserMutationVariables>(KickUserDocument, options);
}
export type KickUserMutationHookResult = ReturnType<typeof useKickUserMutation>;
export type KickUserMutationResult = Apollo.MutationResult<KickUserMutation>;
export type KickUserMutationOptions = Apollo.BaseMutationOptions<
  KickUserMutation,
  KickUserMutationVariables
>;
export const LeaveGroupDocument = gql`
  mutation leaveGroup($groupId: Int!) {
    leave(groupId: $groupId) {
      id
      groups {
        id
        name
        ownerId
        usersWithRank {
          id
          groupId
          rank
          username
          stats {
            work
            break
          }
        }
        invitations(joined: false) {
          id
          user {
            id
            username
          }
        }
      }
    }
  }
`;
export type LeaveGroupMutationFn = Apollo.MutationFunction<
  LeaveGroupMutation,
  LeaveGroupMutationVariables
>;

/**
 * __useLeaveGroupMutation__
 *
 * To run a mutation, you first call `useLeaveGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLeaveGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [leaveGroupMutation, { data, loading, error }] = useLeaveGroupMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *   },
 * });
 */
export function useLeaveGroupMutation(
  baseOptions?: Apollo.MutationHookOptions<LeaveGroupMutation, LeaveGroupMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LeaveGroupMutation, LeaveGroupMutationVariables>(
    LeaveGroupDocument,
    options
  );
}
export type LeaveGroupMutationHookResult = ReturnType<typeof useLeaveGroupMutation>;
export type LeaveGroupMutationResult = Apollo.MutationResult<LeaveGroupMutation>;
export type LeaveGroupMutationOptions = Apollo.BaseMutationOptions<
  LeaveGroupMutation,
  LeaveGroupMutationVariables
>;
export const DeleteInviteDocument = gql`
  mutation deleteInvite($groupId: Int!, $inviteId: Int!) {
    deleteInvite(groupId: $groupId, inviteId: $inviteId) {
      id
      invitations {
        id
        groupId
      }
    }
  }
`;
export type DeleteInviteMutationFn = Apollo.MutationFunction<
  DeleteInviteMutation,
  DeleteInviteMutationVariables
>;

/**
 * __useDeleteInviteMutation__
 *
 * To run a mutation, you first call `useDeleteInviteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteInviteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteInviteMutation, { data, loading, error }] = useDeleteInviteMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *      inviteId: // value for 'inviteId'
 *   },
 * });
 */
export function useDeleteInviteMutation(
  baseOptions?: Apollo.MutationHookOptions<DeleteInviteMutation, DeleteInviteMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<DeleteInviteMutation, DeleteInviteMutationVariables>(
    DeleteInviteDocument,
    options
  );
}
export type DeleteInviteMutationHookResult = ReturnType<typeof useDeleteInviteMutation>;
export type DeleteInviteMutationResult = Apollo.MutationResult<DeleteInviteMutation>;
export type DeleteInviteMutationOptions = Apollo.BaseMutationOptions<
  DeleteInviteMutation,
  DeleteInviteMutationVariables
>;
export const QuestionsDocument = gql`
  mutation questions($timePreference: Int, $breakLength: Int, $typeOfExercise: String) {
    updateQuestions(
      data: {
        timePreference: $timePreference
        breakLength: $breakLength
        typeOfExercise: $typeOfExercise
      }
    ) {
      id
      timePreference
      breakLength
      typeOfExercise
    }
  }
`;
export type QuestionsMutationFn = Apollo.MutationFunction<
  QuestionsMutation,
  QuestionsMutationVariables
>;

/**
 * __useQuestionsMutation__
 *
 * To run a mutation, you first call `useQuestionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useQuestionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [questionsMutation, { data, loading, error }] = useQuestionsMutation({
 *   variables: {
 *      timePreference: // value for 'timePreference'
 *      breakLength: // value for 'breakLength'
 *      typeOfExercise: // value for 'typeOfExercise'
 *   },
 * });
 */
export function useQuestionsMutation(
  baseOptions?: Apollo.MutationHookOptions<QuestionsMutation, QuestionsMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<QuestionsMutation, QuestionsMutationVariables>(
    QuestionsDocument,
    options
  );
}
export type QuestionsMutationHookResult = ReturnType<typeof useQuestionsMutation>;
export type QuestionsMutationResult = Apollo.MutationResult<QuestionsMutation>;
export type QuestionsMutationOptions = Apollo.BaseMutationOptions<
  QuestionsMutation,
  QuestionsMutationVariables
>;
export const UpdateTimePreferenceDocument = gql`
  mutation updateTimePreference($time: Int, $breakLength: Int, $type: String) {
    updateTime(data: { timePreference: $time, breakLength: $breakLength }) {
      id
      timePreference
      breakLength
    }
    updateQuestions(data: { typeOfExercise: $type }) {
      id
      typeOfExercise
    }
  }
`;
export type UpdateTimePreferenceMutationFn = Apollo.MutationFunction<
  UpdateTimePreferenceMutation,
  UpdateTimePreferenceMutationVariables
>;

/**
 * __useUpdateTimePreferenceMutation__
 *
 * To run a mutation, you first call `useUpdateTimePreferenceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTimePreferenceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTimePreferenceMutation, { data, loading, error }] = useUpdateTimePreferenceMutation({
 *   variables: {
 *      time: // value for 'time'
 *      breakLength: // value for 'breakLength'
 *      type: // value for 'type'
 *   },
 * });
 */
export function useUpdateTimePreferenceMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdateTimePreferenceMutation,
    UpdateTimePreferenceMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateTimePreferenceMutation, UpdateTimePreferenceMutationVariables>(
    UpdateTimePreferenceDocument,
    options
  );
}
export type UpdateTimePreferenceMutationHookResult = ReturnType<
  typeof useUpdateTimePreferenceMutation
>;
export type UpdateTimePreferenceMutationResult = Apollo.MutationResult<UpdateTimePreferenceMutation>;
export type UpdateTimePreferenceMutationOptions = Apollo.BaseMutationOptions<
  UpdateTimePreferenceMutation,
  UpdateTimePreferenceMutationVariables
>;
export const UsersDocument = gql`
  query USERS {
    users {
      id
      username
    }
  }
`;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useUsersQuery(
  baseOptions?: Apollo.QueryHookOptions<UsersQuery, UsersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
}
export function useUsersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
}
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = Apollo.QueryResult<UsersQuery, UsersQueryVariables>;
export const StatsDocument = gql`
  query Stats {
    profileStats {
      work {
        now
        before
        countNow
        countBefore
      }
      break {
        now
        before
        countNow
        countBefore
      }
      diff
      diffBefore
    }
  }
`;

/**
 * __useStatsQuery__
 *
 * To run a query within a React component, call `useStatsQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStatsQuery({
 *   variables: {
 *   },
 * });
 */
export function useStatsQuery(
  baseOptions?: Apollo.QueryHookOptions<StatsQuery, StatsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<StatsQuery, StatsQueryVariables>(StatsDocument, options);
}
export function useStatsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<StatsQuery, StatsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<StatsQuery, StatsQueryVariables>(StatsDocument, options);
}
export type StatsQueryHookResult = ReturnType<typeof useStatsQuery>;
export type StatsLazyQueryHookResult = ReturnType<typeof useStatsLazyQuery>;
export type StatsQueryResult = Apollo.QueryResult<StatsQuery, StatsQueryVariables>;

export interface PossibleTypesResultData {
  possibleTypes: {
    [key: string]: string[];
  };
}
const result: PossibleTypesResultData = {
  possibleTypes: {},
};
export default result;
