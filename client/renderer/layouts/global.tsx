import React from "react";
import { Box, Heading, VStack } from "@chakra-ui/react";

export const GlobalLayout: React.FC<{}> = ({ children }) => {
  return (
    <>
      <Box
        w="100vw"
        h="100vh"
        borderRadius="5px"
        shadow="5px 5px 15px 4px rgba(117,35,150,0.43)"
        backgroundColor="#gray.900"
        alignItems="center"
        display="flex"
        justifyContent="center"
      >
        <VStack spacing="3">
          <Heading as="h1" fontSize={["2em", "4em"]} color="white">
            Standy
          </Heading>
          <VStack>
            <Heading color="purple.100">Self-care means giving yourself</Heading>
            <Heading color="purple.100">permission to pause!</Heading>

            {children}
          </VStack>
        </VStack>
      </Box>
    </>
  );
};
