import { BellIcon, CheckIcon, SettingsIcon } from "@chakra-ui/icons";
import { Box, Flex, Heading, HStack, SimpleGrid, Spinner } from "@chakra-ui/react";
import { LinkIconButton } from "chakra-next-link";
import { useMeQuery } from "generated/graphql";
import Link from "next/link";
import React from "react";
import { BiGroup } from "react-icons/bi";
import { FiHome } from "react-icons/fi";
import { IoIosFitness } from "react-icons/io";
import { CgProfile } from "react-icons/cg";

export const Header: React.FC<{
  title: string | React.ReactNode;
  home?: boolean;
  pathBack?: string;
  selectedIcon?: string;
  isNotScroll?: boolean;
  isUsername?: boolean;
}> = ({ isUsername, title, isNotScroll, selectedIcon, pathBack, children }) => {
  const { data, loading, error } = useMeQuery();

  if (error) {
    return <Heading>Error...</Heading>;
  }

  return (
    <Box minW="100vw" minH="100vh" backgroundColor="gray.800" justifyContent="center">
      <Box maxW="100vw">
        {loading || !data?.me ? (
          <Spinner />
        ) : (
          <Box h="100vh">
            <SimpleGrid columns={3} backgroundColor="whiteAlpha.200" w="full" h="80px">
              <HStack h="full" alignItems="center" spacing="3" ml="3">
                <LinkIconButton
                  aria-label="groups"
                  icon={<BiGroup />}
                  href="/home/groups"
                  variant={selectedIcon === "groups" ? undefined : "outline"}
                  colorScheme={selectedIcon === "groups" ? undefined : "whiteAlpha"}
                />

                <LinkIconButton
                  aria-label="exercise"
                  icon={<IoIosFitness />}
                  href="/home/exercises"
                  variant={selectedIcon === "exercise" ? undefined : "outline"}
                  colorScheme={selectedIcon === "exercise" ? undefined : "whiteAlpha"}
                />
                <LinkIconButton
                  aria-label="goals"
                  icon={<CheckIcon />}
                  href="/home/goals"
                  variant={selectedIcon === "goals" ? undefined : "outline"}
                  colorScheme={selectedIcon === "goals" ? undefined : "whiteAlpha"}
                />
                {/* <LinkIconButton icon={<ArrowBackIcon />} aria-label="Back" href={pathBack} /> */}
              </HStack>
              {/* {home && (
                <Box mt="5" ml="14" pos="absolute">
                  <LinkIconButton icon={<FiHome />} aria-label="Home" href="/home" />
                </Box>
              )} */}
              <Flex height="full" w="full" alignItems="center" justifyContent="center">
                <Heading
                  color="white"
                  textDecoration={selectedIcon === "username" ? "underline" : undefined}
                  cursor={isUsername && "pointer"}
                >
                  {isUsername ? (
                    <Box _hover={{ color: "gray" }}>
                      <Link href="/home/profile">{title}</Link>
                    </Box>
                  ) : (
                    title
                  )}
                </Heading>
              </Flex>

              <HStack h="full" justifyContent="flex-end" alignItems="center" spacing="3" mr="3">
                {data.me.invitations.length > 0 ? (
                  <LinkIconButton
                    aria-label="notifications"
                    href="/home/notifications"
                    overflow="visible"
                    _after={{
                      content: `"${data.me.invitations.length}"`,
                      fontSize: "10px",
                      lineHeight: "10px",
                      color: "white",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      pos: "absolute",
                      top: -1,
                      right: -1,
                      h: 4,
                      w: 4,
                      borderRadius: "50%",
                      bgGradient: "linear(to-r, #dc004e, #f44336)",
                    }}
                    variant={selectedIcon === "notification" ? undefined : "outline"}
                    colorScheme={selectedIcon === "notification" ? undefined : "whiteAlpha"}
                  >
                    <BellIcon />
                  </LinkIconButton>
                ) : (
                  <LinkIconButton
                    aria-label="notifications"
                    href="/home/notifications"
                    overflow="visible"
                    variant={selectedIcon === "notification" ? undefined : "outline"}
                    colorScheme={selectedIcon === "notification" ? undefined : "whiteAlpha"}
                  >
                    <BellIcon />
                  </LinkIconButton>
                )}
                <LinkIconButton
                  aria-label="profile"
                  icon={<CgProfile />}
                  href="/home/profile"
                  variant={selectedIcon === "profile" ? undefined : "outline"}
                  colorScheme={selectedIcon === "profile" ? undefined : "whiteAlpha"}
                />

                <LinkIconButton
                  icon={<FiHome />}
                  aria-label="Home"
                  href="/home"
                  variant={selectedIcon === "home" ? undefined : "outline"}
                  colorScheme={selectedIcon === "home" ? undefined : "whiteAlpha"}
                />
                {/* <LinkIconButton icon={<ArrowBackIcon />} aria-label="Back" href={pathBack} /> */}
              </HStack>
            </SimpleGrid>

            {/* overflowY="scroll" */}
            <Box
              h="calc(100vh - 80px)"
              pt="2"
              className="fancyScroll"
              overflowY={isNotScroll ? "hidden" : "scroll"}
            >
              {children}
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};
