import { Switch, SwitchProps } from "@chakra-ui/react";
import React from "react";

export const Toggle: React.FC<SwitchProps> = ({ children, ...rest }) => {
  return (
    <Switch d="flex" {...rest}>
      {children}
    </Switch>
  );
};
