import React, { FunctionComponent } from "react";
import { Heading, VStack } from "@chakra-ui/react";

type QuestionProps = {
  title: string;
};

const QuestionContent: FunctionComponent<QuestionProps> = ({ title, children }) => {
  return (
    <VStack mt="2" spacing="4" maxW="80%" marginX="auto" textAlign="center">
      <Heading fontSize="1.3em" color="white">
        {title}
      </Heading>

      {children}
    </VStack>
  );
};

export { QuestionContent };
