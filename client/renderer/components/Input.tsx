import React, { forwardRef } from "react";
import { useFormContext, ValidationRule } from "react-hook-form";
import {
  Input as ChakraInput,
  InputProps as ChakraInputProps,
  Textarea as ChakraTextarea,
  TextareaProps as ChakraTextareaProps,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormControlProps,
} from "@chakra-ui/react";
import { capitalize } from "lodash";

const allowed_types = [
  "text",
  "password",
  "current_password",
  "confirm_password",
  "email",
  "number",
] as const;

type AllowedTypes = typeof allowed_types[number];

export interface TextAreaProps extends ChakraTextareaProps {
  label?: string;
  noLabel?: boolean;
  error?: string;
  name?: string;
  outerProps?: FormControlProps;
}

export interface InputProps extends Omit<ChakraInputProps, "type"> {
  label?: string;
  noLabel?: boolean;
  error?: string;
  name?: string;
  type?: AllowedTypes | string;
  outerProps?: FormControlProps;
}

function isSpecifiedType(name: string): name is AllowedTypes {
  return (allowed_types as any).includes(name);
}

const resoleType = (name: string) => {
  if (isSpecifiedType(name)) {
    if (name === "confirm_password" || name === "current_password") {
      return "password";
    }

    return name;
  }

  return "text";
};

export const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>((props, ref) => {
  const {
    name: baseName = "TextAreaField Field",
    isInvalid,
    isRequired,
    label,
    placeholder,
    outerProps,
    noLabel,
    ...rest
  } = props;

  const name = baseName.split("_").join(" ");

  return (
    <FormControl isInvalid={isInvalid || !!props.error} isRequired={isRequired} {...outerProps}>
      {!noLabel && <FormLabel htmlFor={baseName}>{label || capitalize(name)}</FormLabel>}
      <ChakraTextarea
        type={resoleType(baseName)}
        id={baseName}
        name={baseName}
        ref={ref}
        placeholder={placeholder || label || capitalize(name)}
        {...rest}
      />
      <FormErrorMessage>{props.error}</FormErrorMessage>
    </FormControl>
  );
});

export const TextAreaField = forwardRef<HTMLTextAreaElement, TextAreaProps>((props, ref) => {
  const { register, formState, errors } = useFormContext();
  const baseName = props.name || "Unknown name";
  const name = baseName.split("_").join(" ");

  const error = Array.isArray(errors[name])
    ? errors[name].join(", ")
    : errors[name]?.message || errors[name];

  return (
    <TextArea
      {...props}
      name={baseName}
      outerProps={{ isDisabled: formState.isSubmitting }}
      error={error}
      ref={register()}
    />
  );
});

export const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  const {
    name: baseName = "Input Field",
    isInvalid,
    isRequired,
    label,
    placeholder,
    outerProps,
    noLabel,
    ...rest
  } = props;

  const name = baseName.split("_").join(" ");

  return (
    <FormControl isInvalid={isInvalid || !!props.error} isRequired={isRequired} {...outerProps}>
      {!noLabel && <FormLabel htmlFor={baseName}>{label || capitalize(name)}</FormLabel>}
      <ChakraInput
        type={resoleType(baseName)}
        id={baseName}
        name={baseName}
        ref={ref}
        placeholder={placeholder || label || capitalize(name)}
        {...rest}
      />
      <FormErrorMessage>{props.error}</FormErrorMessage>
    </FormControl>
  );
});

export const InputField = forwardRef<
  HTMLInputElement,
  InputProps & { validationPattern?: ValidationRule<RegExp> }
>((props, ref) => {
  const { register, formState, errors } = useFormContext();
  const baseName = props.name || "Unknown name";
  const name = baseName.split("_").join(" ");

  const error = Array.isArray(errors[name])
    ? errors[name].join(", ")
    : errors[name]?.message || errors[name];

  return (
    <Input
      {...props}
      name={baseName}
      outerProps={{ isDisabled: formState.isSubmitting }}
      error={error}
      ref={register({
        pattern: props.validationPattern,
        // valueAsNumber: true
        setValueAs: (val) => {
          if (props.type === "number") {
            if (val.length === 0 && !props.isRequired) {
              return undefined;
            }

            return parseFloat(val);
          }

          return val;
        },
      })}
    />
  );
});
