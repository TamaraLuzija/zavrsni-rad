import React, { useEffect } from "react";
import { Flex, SimpleGrid, theme } from "@chakra-ui/react";
import { CheckCircleIcon } from "@chakra-ui/icons";

const ignoreColors = ["transparent", "current", "whiteAlpha", "blackAlpha"] as const;
type Colors = Exclude<keyof typeof theme.colors, typeof ignoreColors[number]>;

const colorsObj = Object.keys(theme.colors)
  .filter((a) => !ignoreColors.includes(a as any))
  .reduce((obj, color) => {
    obj[color] = color;
    return obj;
  }, {} as Record<Colors, string>);

export const colors = Object.keys(colorsObj);
export const noNumber = ["current", "white", "black"];
const TAG_CARD_COLOR_VARIANT = "400";

export const SelectColor: React.FC<{
  name: string;
  variant?: number;
  color: string;
  setColor: React.Dispatch<React.SetStateAction<string>>;
}> = ({ name, color: selectedColor, setColor, variant }) => {
  useEffect(() => {
    if (selectedColor === undefined) {
      setColor(colors[Math.floor(Math.random() * colors.length)]);
    }
  }, [selectedColor]);

  return (
    <SimpleGrid columns={[3, 9]} gap="1">
      {colors.map((color) => {
        const hasPalette = !noNumber.includes(color);
        const bg = `${color}${hasPalette ? `.${variant || TAG_CARD_COLOR_VARIANT}` : ""}`;
        const check = hasPalette ? `${color}.800` : "red.500";

        return (
          <Flex
            border={color === "white" ? "1px solid #adadad" : ""}
            bg={bg}
            h="4"
            w="8"
            justifyContent="center"
            alignItems="center"
            pos="relative"
            cursor="pointer"
            onClick={() => setColor(color)}
          >
            {selectedColor === color && <CheckCircleIcon h="5" w="5" color={check} />}
          </Flex>
        );
      })}
    </SimpleGrid>
  );
};
