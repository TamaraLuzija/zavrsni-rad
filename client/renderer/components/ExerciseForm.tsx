import { Box, Flex, VStack, HStack, Heading, Input, Button } from "@chakra-ui/react";

import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { TextArea } from "./Input";
import { SelectColor } from "./SelectColors";

export type ExFormData = { name: string; description?: string; link?: string };

export const ExerciseForm: React.FC<{
  init?: ExFormData;
  onSubmit: (data: ExFormData) => Promise<void>;
}> = ({ init, onSubmit }) => {
  const { register, handleSubmit } = useForm({ defaultValues: { ...init } });

  return (
    <Box h="full" w="full" mt="2" overflowY="hidden">
      <form
        onSubmit={handleSubmit((data) => onSubmit({ ...data }))}
        style={{ width: "100%", height: "100%" }}
      >
        <Flex flexDir="column" w="full" h="full" alignItems="center">
          <VStack align="flex-start" w="70%">
            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Name:
              </Heading>
              <Input ref={register} name="name" color="white" noLabel />
            </HStack>
            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Description:
              </Heading>
              <TextArea ref={register} name="description" color="white" noLabel />
            </HStack>

            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Link:
              </Heading>
              <Input ref={register} name="link" h="20px" color="white" noLabel />
            </HStack>
          </VStack>
          <Button
            type="submit"
            colorScheme="blue"
            size="md"
            width="20%"
            ml="10"
            mb="4"
            pos="absolute"
            bottom="-2"
            right="8"
          >
            {init?.name ? "Update" : "Create"}
          </Button>
        </Flex>
      </form>
    </Box>
  );
};
