import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  useDisclosure,
  useToast,
  VStack,
} from "@chakra-ui/react";

import { useUpdateTimePreferenceMutation } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import { Num } from "./Num";
import { BiDotsHorizontal } from "react-icons/bi";

export const TimerComponent: React.FC<{
  timePreference?: number;
  breakLength?: number;
}> = ({ timePreference, breakLength }) => {
  const [session, setSession] = useState(timePreference);
  const [breakTime, setBreakTime] = useState(breakLength);

  const [updateTime, { loading }] = useUpdateTimePreferenceMutation();
  const toast = useToast();

  useEffect(() => {
    setBreakTime(breakLength);
  }, [breakLength]);

  useEffect(() => {
    setSession(timePreference);
  }, [timePreference]);

  const disclosureProps = useDisclosure();
  const onSave = async (event: React.FormEvent) => {
    event.preventDefault();

    await updateTime({
      variables: {
        time: session,
        breakLength: breakTime,
      },
      refetchQueries: [{ query: ME }],
    });
    event.preventDefault();

    toast({ title: "Yay, updated", status: "success" });
    disclosureProps.onClose();
  };

  return (
    <Popover placement="bottom" closeOnBlur={true} {...disclosureProps}>
      <PopoverTrigger>
        <IconButton
          aria-label="open"
          icon={<BiDotsHorizontal />}
          onClick={disclosureProps.onToggle}
        />
      </PopoverTrigger>
      <PopoverContent bg="gray.900">
        <form onSubmit={onSave}>
          <PopoverHeader fontWeight="bold" color="white">
            Manage Your Activity
          </PopoverHeader>
          <PopoverArrow />
          <PopoverCloseButton />
          <PopoverBody>
            <Flex w="full" h="full" justify="space-between" align="flex-end">
              <VStack>
                <HStack align="center">
                  <Heading fontSize="1rem" color="white">
                    Session length:
                  </Heading>
                  <Num width={20} value={session} setValue={(val) => setSession(val)} withButtons />
                </HStack>
                <HStack mt="4">
                  <Heading fontSize="1rem" color="white" mr="4">
                    Break length:
                  </Heading>
                  <Num
                    width={20}
                    value={breakTime}
                    setValue={(val) => setBreakTime(val)}
                    withButtons
                  />
                </HStack>
              </VStack>
              <Box>
                <Button colorScheme="blue" type="submit" isLoading={loading}>
                  Save
                </Button>
              </Box>
            </Flex>
          </PopoverBody>
        </form>
      </PopoverContent>
    </Popover>
  );
};
