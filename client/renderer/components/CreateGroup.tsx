import {
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spinner,
  UseDisclosureReturn,
} from "@chakra-ui/react";
import { useUsersQuery, useCreateGroupMutation } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import React, { useEffect, useState } from "react";
import Select from "react-select";

export const CreateNewGroup: React.FC<{ userId: number; modalProps: UseDisclosureReturn }> = ({
  userId,
  modalProps,
}) => {
  const { data, error, loading } = useUsersQuery();

  const [name, setName] = useState("");
  const [userIds, setUserIds] = useState<number[]>([]);

  const [createGroup] = useCreateGroupMutation();

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      await createGroup({
        variables: { name, membersIds: userIds },
        refetchQueries: [{ query: ME }],
        awaitRefetchQueries: true,
      });
      modalProps.onClose();
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setName("");
    setUserIds([]);
  }, [modalProps.isOpen]);

  return (
    <Modal {...modalProps}>
      <ModalOverlay />
      <ModalContent backgroundColor="gray.100">
        <form onSubmit={onSubmit}>
          <ModalHeader m="0 auto" fontWeight="bold" color="gray.600">
            Create new group
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl mt="-4">
              <FormLabel fontWeight="bold" color="gray.600">
                Group name
              </FormLabel>
              <Input
                isRequired
                placeholder="Group name"
                borderColor="gray.600"
                color="gray.600"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel fontWeight="bold" color="gray.600">
                Add members
              </FormLabel>
              {loading ? (
                <Spinner />
              ) : error || !data ? (
                <Heading>Error...</Heading>
              ) : (
                <Select
                  borderColor="gray.600"
                  isMulti
                  value={data.users
                    .filter((u) => u.id !== userId)
                    .filter((u) => userIds.includes(u.id))
                    .map((u) => ({ value: u.id, label: u.username }))}
                  options={data.users
                    .filter((u) => u.id !== userId)
                    .map((user) => ({ value: user.id, label: user.username }))}
                  onChange={(selected) => {
                    setUserIds(selected.map((v) => v.value));
                  }}
                />
              )}
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="orange" mr={3} type="submit">
              Create
            </Button>
            <Button onClick={modalProps.onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};
