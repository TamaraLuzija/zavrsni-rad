import {
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
} from "@chakra-ui/react";
import React from "react";

export const Num: React.FC<{
  value: number;
  setValue: (val: number) => void;
  width?: number;
  withButtons?: boolean;
  limits?: {
    max?: number;
    min?: number;
  };
}> = ({ value, setValue, width, withButtons, limits }) => {
  return (
    <NumberInput
      value={value}
      {...(limits?.min !== undefined && { min: limits.min })}
      {...(limits?.max !== undefined && { max: limits.max })}
      color="white"
      onChange={(e) => setValue(parseInt(e))}
      width={width}
    >
      <NumberInputField fontWeight="bold" fontSize="1em" />
      {withButtons && (
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      )}
    </NumberInput>
  );
};
