import { IpcRenderer } from "electron";

export const useElectron = () => {
  if (typeof window === "undefined") {
    return null;
  }

  const { ipcRenderer } = window.require("electron");
  return ipcRenderer as IpcRenderer;
};
