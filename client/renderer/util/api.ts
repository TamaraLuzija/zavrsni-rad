import { parseCookies } from "nookies";

type Init<B> = Omit<RequestInit, "body"> & { rawBody?: boolean; body?: B };
type C = Record<string, any>;

const fetcher = async <T, B = any>(
  baseUrl: string | undefined,
  url: string,
  rawInit: Init<B>,
  cookies: C | undefined,
  token: boolean
): Promise<T> => {
  cookies = cookies || parseCookies();
  const { rawBody, body, ...init } = rawInit || {};

  const preparedBody: Partial<RequestInit> = {};
  if (body && rawBody) {
    preparedBody.body = body as any;
  } else if (["POST", "PATCH", "PUT", "DELETE"].includes(init.method || "")) {
    preparedBody.body = JSON.stringify(body || {});
  }

  const headers = new Headers({
    "content-type": "application/json",
    ...(token && cookies.token && { Authorization: `Bearer ${cookies.token}` }),
  });

  const res = await fetch(`${baseUrl}${url}`, {
    ...init,
    ...preparedBody,
    headers,
  });
  const data = await res.json();

  if (!res.ok) {
    throw data;
  }

  return data;
};

export const api = async <T, Body = C>(
  url: string,
  rawInit: Init<Body>,
  cookies?: C,
  token = true
): Promise<T> => {
  return fetcher("http://localhost:4000/api/v1", url, rawInit, cookies, token);
};
