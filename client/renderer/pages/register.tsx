import React, { useState } from "react";
import { Box, Heading, VStack } from "@chakra-ui/layout";
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button,
  CloseButton,
  HStack,
  SimpleGrid,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { Link } from "chakra-next-link";
import { useForm } from "react-hook-form";

import { Input } from "../components/Input";
import { useMeLazyQuery, useSignUpMutation } from "../generated/graphql";
import { useApolloClient } from "@apollo/client";
import { ME } from "graphql/auth/queries";

interface FormData {
  username: string;
  email: string;
  password: string;
  confirm_password: string;
}

const RegisterPage = () => {
  const router = useRouter();
  const [globalError, setGlobalError] = useState<any>();
  const { register, handleSubmit, setError, errors } = useForm<FormData>();
  const [signUp] = useSignUpMutation();

  const {query} = useApolloClient();

  const onSubmit = async (data: FormData) => {
    if (data.password.length < 4) {
      setError("password", { message: "Password too short" });
      return;
    }
    if (data.password !== data.confirm_password) {
      setError("confirm_password", { message: "Passwords don't match" });
      return;
    }

    try {
      localStorage.removeItem("token");
      const res = await signUp({ variables: { ...data } });
      const { token } = res.data.signUp;
      localStorage.setItem("token", token);
      await query({query: ME});
      await router.push("/questions");
    } catch (error) {
      if (error.err) {
        switch (error.message) {
          case "EMAIL_IN_USE":
            setError("email", { message: "Email already in use" });
            break;
          case "USERNAME_IN_USE":
            setError("username", { message: "Username already in use" });
            break;
          default:
            setGlobalError(error.message);
        }
        return;
      }

      setGlobalError("Unknown error occurred");
      console.error(error);
    }
  };

  return (
    <>
      <Box w="100vw" h="100vh" backgroundColor="gray.700" display="flex">
        <HStack>
          <Heading
            fontSize="3em"
            color="white"
            textAlign="center"
            transform="rotate(-90deg)"
            ml="-8"
          >
            Register
          </Heading>
          <VStack
            width="400px"
            backgroundColor="purple.50"
            borderRadius="5px"
            position="relative"
            p="2"
          >
            {globalError && (
              <Alert status="error">
                <AlertIcon />
                <AlertTitle mr={2}>Error occured</AlertTitle>
                <AlertDescription>{globalError}</AlertDescription>
                <CloseButton
                  onClick={() => setGlobalError(undefined)}
                  position="absolute"
                  right="8px"
                  top="8px"
                />
              </Alert>
            )}
            <form onSubmit={handleSubmit(onSubmit)}>
              <VStack mx="4" spacing="6">
                <SimpleGrid columns={2} w="full" gap="3">
                  <Input error={errors["email"]?.message} ref={register()} name="email" />
                  <Input error={errors["username"]?.message} ref={register()} name="username" />
                  <Input error={errors["password"]?.message} ref={register()} name="password" />
                  <Input
                    error={errors["confirm_password"]?.message}
                    ref={register()}
                    name="confirm_password"
                  />
                </SimpleGrid>

                <Button type="submit" colorScheme="teal" px="14">
                  Register Now
                </Button>
              </VStack>
            </form>
            <Link href="/login">Go back to login</Link>
          </VStack>
        </HStack>
      </Box>
    </>
  );
};

export default RegisterPage;
