import React, { createContext, useContext, useState } from "react";
import { Box, Heading, HStack, VStack } from "@chakra-ui/layout";
import StepProgressBar from "react-step-progress";
import { Radio, RadioGroup, useToast, Spinner } from "@chakra-ui/react";
import "react-step-progress/dist/index.css";

import { QuestionContent } from "../components/QuestionContent";
import { useMeQuery, useQuestionsMutation } from "../generated/graphql";
import { useRouter } from "next/router";
import { Num } from "../components/Num";

const RadioSelect = ({ options, selected, setSelected }) => {
  console.log(selected);

  return (
    <RadioGroup value={selected} color="white" fontWeight="bold" onChange={setSelected}>
      <HStack spacing="3" backgroundColor="whiteAlpha.300" p="4" borderRadius="5px">
        {options.map((option) => (
          <Radio value={option} colorScheme="green">
            {option}
          </Radio>
        ))}
      </HStack>
    </RadioGroup>
  );
};

export type Store = { minutes: number; breakLength: number; type: "walk" | "exercise" };
type ContextType = {
  store: Store;
  setStore: React.Dispatch<React.SetStateAction<Store>>;
};
const initStore = { minutes: 60, breakLength: 8, type: "walk" } as const;
const Context = createContext<ContextType>({ store: initStore, setStore: () => {} });

const FirstStep = () => {
  const { store, setStore } = useContext(Context);

  return (
    <QuestionContent title="How many minutes do you want to work in one session?">
      <Num
        value={store.minutes || 60}
        setValue={(minutes) => setStore((s) => ({ ...s, minutes }))}
        width={24}
        limits={{ min: 0, max: 200 }}
        withButtons
      />
    </QuestionContent>
  );
};

const SecondStep = () => {
  const { store, setStore } = useContext(Context);

  return (
    <QuestionContent title="How many minutes should the breaks between the two sessions be?">
      <Num
        value={store.breakLength || 60}
        setValue={(breakLength) => setStore((s) => ({ ...s, breakLength }))}
        width={24}
        limits={{ min: 0, max: 200 }}
        withButtons
      />
    </QuestionContent>
  );
};

const ThirdStep = () => {
  const { store, setStore } = useContext(Context);

  return (
    <QuestionContent title="What do you prefer?">
      <RadioSelect
        options={["exercise", "walk"]}
        selected={store.type}
        setSelected={(opt) => setStore((s) => ({ ...s, type: opt }))}
      />
    </QuestionContent>
  );
};

const Questions = () => {
  const [updateQuestions] = useQuestionsMutation();
  const { data, loading } = useMeQuery();
  const [store, setStore] = useState<Store>({
    timePreference: data?.me?.timePreference || initStore.minutes,
    // @ts-ignore
    type: data?.me?.typeOfExercise || initStore.type,
    // @ts-ignore
    work: data?.me?.workplace || initStore.work,
  });

  const router = useRouter();
  const toast = useToast();

  const onSubmit = async () => {
    await updateQuestions({
      variables: {
        typeOfExercise: store.type,
        breakLength: store.breakLength,
        timePreference: store.minutes,
      },
    });

    toast({ title: "Saved", status: "success" });

    await router.push("/home");
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <Context.Provider value={{ store, setStore }}>
      <Box w="100vw" h="100vh" backgroundColor="gray.900" display="flex" justifyContent="center">
        <VStack spacing="2">
          <Heading as="h3" fontSize="2em" color="white" m="10px auto">
            Welcome to Standy
          </Heading>
          <StepProgressBar
            startingStep={0}
            wrapperClass="wrapper"
            onSubmit={onSubmit}
            progressClass="progressClassChanges"
            stepClass="stepClassChanges"
            steps={[
              {
                label: "Step 1",
                name: "step 1",
                content: <FirstStep />,
                validator: () => true,
              },
              {
                label: "Step 2",
                name: "step 2",
                content: <SecondStep />,
                validator: () => true,
              },
              {
                label: "Step 3",
                name: "step 3",
                content: <ThirdStep />,
                validator: () => true,
              },
            ]}
            nextBtnName="Continue"
            submitBtnName="Continue"
            secondaryBtnClass="previousButton"
            primaryBtnClass="nextButton"
            buttonWrapperClass="buttonWrapperClassChanges"
          />
        </VStack>
      </Box>
    </Context.Provider>
  );
};

export default Questions;
