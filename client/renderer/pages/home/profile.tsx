import React, { useEffect } from "react";
import {
  Box,
  Flex,
  IconButton,
  Spinner,
  Stat,
  StatArrow,
  StatGroup,
  StatHelpText,
  StatLabel,
  StatNumber,
  VStack,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { SettingsIcon } from "@chakra-ui/icons";
import { LinkButton } from "chakra-next-link";
import { BiLogOut } from "react-icons/bi";

import { useMeQuery, useStatsQuery } from "../../generated/graphql";
import { Header } from "components/Header";

const roundPercent = (n: number) => {
  return Math.round(n * 10000) / 100;
};

const RenderThingy: React.FC<{ name: string; before: number; now: number }> = ({
  name,
  before,
  now,
}) => {
  return (
    <Stat w="full" display="flex" justifyContent="center">
      <StatLabel color="white">{name}</StatLabel>
      <StatNumber>{now}</StatNumber>
      {now !== before && (
        <StatHelpText>
          <StatArrow type={now > before ? "increase" : "decrease"} />
          {before === 0 ? 0 : roundPercent(now / before)}%
        </StatHelpText>
      )}
    </Stat>
  );
};

const ProfilePage = () => {
  const { data, loading: meLoading, error: meError, refetch } = useMeQuery();
  const { data: s, loading: sLoading, error: sError, refetch: refetchThingy } = useStatsQuery();
  const router = useRouter();

  const logout = async () => {
    localStorage.removeItem("token");
    await refetch();
    await router.push("/");
  };

  useEffect(() => {
    if (!meLoading) {
      if (!data?.me) {
        router.push("/");
      } else {
        refetchThingy();
      }
    }
  }, [meLoading, data]);

  return (
    <Header title={data?.me?.username} selectedIcon="profile" isNotScroll>
      {sLoading || meLoading || !s?.profileStats ? (
        <Spinner />
      ) : sError || meError ? (
        <h1>Error...</h1>
      ) : (
        <>
          <VStack w="full" h="full" display="flex" spacing="12">
            <StatGroup w="full" color="white" mt="8">
              <RenderThingy
                name="Sessions (h)"
                now={s.profileStats.work.now / 60}
                before={s.profileStats.work.before / 60}
              />
              <RenderThingy
                name="Breaks (h)"
                now={s.profileStats.break.now / 60}
                before={s.profileStats.break.before / 60}
              />
              <RenderThingy
                name="Session count"
                now={s.profileStats.work.countNow}
                before={s.profileStats.work.countBefore}
              />
              <RenderThingy
                name="Work count"
                now={s.profileStats.break.countNow}
                before={s.profileStats.break.countBefore}
              />
            </StatGroup>

            <Flex w="200px" px="10">
              <LinkButton
                href="/home/settings"
                aria-label="settings"
                backgroundColor="white"
                width="20px"
                mr="4"
              >
                <SettingsIcon color="black" />
              </LinkButton>
              <IconButton
                icon={<BiLogOut />}
                aria-label="logout"
                onClick={logout}
                colorScheme="red"
                width="20px"
                mb="2"
              />
            </Flex>
          </VStack>
        </>
      )}
    </Header>
  );
};

export default ProfilePage;
