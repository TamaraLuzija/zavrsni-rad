import React from "react";
import { StarIcon } from "@chakra-ui/icons";
import { Spinner, Heading, VStack, Text, Box, Flex, HStack, SimpleGrid } from "@chakra-ui/react";

import { useUserAchievementsQuery } from "generated/graphql";
import { Header } from "components/Header";

const GroupsPage = () => {
  const {
    data: allGoalsData,
    loading: allGoalsLoading,
    error: allGoalsError,
  } = useUserAchievementsQuery();

  return (
    <Header
      title={
        <HStack spacing="4">
          <Text>My goals</Text>
        </HStack>
      }
      selectedIcon="goals"
    >
      {allGoalsLoading ? (
        <Spinner color="white" />
      ) : allGoalsError ? (
        <Heading>Error...</Heading>
      ) : (
        <SimpleGrid columns={2} gap="2" direction="column" w="90%" m="6px auto">
          {/* {goalsData.userAchievementType.types.map((userAchievementType) => */}
          {allGoalsData.userAchievements.map((achievement) => (
            <VStack
              backgroundColor="whiteAlpha.700"
              color="blue.800"
              borderRadius="5px"
              fontWeight="bold"
              fontSize="1.4em"
              pos="relative"
            >
              <Box
                display="flex"
                width="100%"
                justifyContent="center"
                backgroundColor="whiteAlpha.400"
                borderTopRightRadius="5px"
                borderTopLeftRadius="5px"
              >
                <Heading href={`/home/goals/show?id=${achievement.id}`} fontSize="0.8em">
                  {achievement.name}
                </Heading>
              </Box>

              <Flex w="100%" justifyContent="center" px="1">
                <Heading fontSize="1em" color="purple.800" px="2">
                  {achievement.currentValue} / {achievement.nextValue}
                </Heading>

                <HStack mr="1" px="2">
                  {Array.from(new Array(achievement.maxLevel), (d, index) => (
                    <StarIcon
                      boxSize="18px"
                      color={index <= achievement.userLevel ? "yellow.600" : "gray.300"}
                    />
                  ))}
                </HStack>
              </Flex>

              <Heading p="2" fontSize=".6em" color="green.800">
                {achievement.message}
              </Heading>
            </VStack>
          ))}
        </SimpleGrid>
      )}
    </Header>
  );
};

export default GroupsPage;
