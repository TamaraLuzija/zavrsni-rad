import React from "react";
import { Heading } from "@chakra-ui/layout";
import { Link } from "chakra-next-link";

import { useActivitiesQuery } from "generated/graphql";
import { Code, ListItem, UnorderedList } from "@chakra-ui/react";
import { Icon } from "@chakra-ui/icons";
import { CgWorkAlt } from "react-icons/cg";
import { RiRestTimeLine } from "react-icons/ri";

const Activities = () => {
  const { data, loading, error } = useActivitiesQuery();

  if (loading) {
    return (
      <h1>
        <Link href="/home">Back</Link> Loading...
      </h1>
    );
  }

  if (error) {
    return (
      <h1>
        <Link href="/home">Back</Link> Error...
      </h1>
    );
  }

  return (
    <UnorderedList>
      <Link href="/home">Back</Link>
      {data.activities.map((activity) => (
        <ListItem key={activity.id}>
          <Heading size="md">
            <Icon
              color={activity.complete ? "green.400" : undefined}
              as={activity.isBreak ? RiRestTimeLine : CgWorkAlt}
            />
            <Code mr="2">{activity.date.toLocaleString()}</Code>
            {activity.id} {"->"} {Math.floor(activity.time / 60)}min /{" "}
            {Math.floor(activity.fullTime / 60)}min
          </Heading>
        </ListItem>
      ))}
    </UnorderedList>
  );
};

export default Activities;
