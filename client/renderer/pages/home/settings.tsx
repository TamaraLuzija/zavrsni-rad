import React, { useEffect, useState } from "react";
import {
  Heading,
  Button,
  Box,
  HStack,
  useToast,
  Flex,
  Radio,
  RadioGroup,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import { useRouter } from "next/router";

import { useMeQuery, useUpdateTimePreferenceMutation } from "../../generated/graphql";
import { Header } from "components/Header";
import { Num } from "components/Num";
import { ME } from "graphql/auth/queries";

const Settings = () => {
  const { data, loading: meLoading } = useMeQuery();
  const router = useRouter();
  const toast = useToast();

  const [typeOfExercise, setTypeOfExercise] = useState("exercise");
  const [session, setSession] = useState(60);
  const [breakTime, setBreakTime] = useState(5);

  useEffect(() => {
    if (!meLoading) {
      if (!data.me) router.push("/");
      else {
        setSession(data.me.timePreference);
        setBreakTime(data.me.breakLength);
        setTypeOfExercise(data.me.typeOfExercise);
      }
    }
  }, [meLoading, data]);

  const [updateTime, { loading }] = useUpdateTimePreferenceMutation();

  const onSave = async (event: React.FormEvent) => {
    event.preventDefault();

    await updateTime({
      variables: {
        time: session,
        breakLength: breakTime,
        type: typeOfExercise,
      },
      refetchQueries: [{ query: ME }],
    });

    toast({ title: "Yay, updated", status: "success", duration: 1000 });
    return router.push("/home");
  };

  const changeTime =
    data?.me?.timePreference !== session ||
    data?.me?.breakLength !== breakTime ||
    data?.me.typeOfExercise !== typeOfExercise;

  return (
    <Header title="Settings" selectedIcon="profile" isNotScroll>
      <form onSubmit={onSave}>
        <Box>
          <Button
            mr="4"
            mt="4"
            float="right"
            alignItems="center"
            colorScheme="blue"
            type="submit"
            isLoading={loading}
            isDisabled={!changeTime}
          >
            Save
          </Button>
          <Grid templateColumns="100px 1fr" pl="4">
            <GridItem>
              <Flex align="center" h="full">
                <Heading fontSize="1.2em" color="white">
                  Type:
                </Heading>
              </Flex>
            </GridItem>

            <GridItem>
              <Flex align="center" h="full">
                <RadioGroup
                  value={typeOfExercise}
                  color="white"
                  fontWeight="bold"
                  onChange={(e) => setTypeOfExercise(e as any)}
                >
                  <Box spacing="3" p="2" borderRadius="5px">
                    <HStack>
                      {["exercise", "walk"].map((option) => (
                        <Radio value={option} colorScheme="green">
                          {option}
                        </Radio>
                      ))}
                    </HStack>
                  </Box>
                </RadioGroup>
              </Flex>
            </GridItem>

            <GridItem>
              <Flex align="center" h="full">
                <Heading fontSize="1.2em" color="white">
                  Session:{" "}
                </Heading>
              </Flex>
            </GridItem>

            <GridItem>
              <Flex align="center" h="full">
                <Box p="2" justifyContent="center" alignItems="center">
                  <Num
                    width={20}
                    limits={{ min: 1 }}
                    value={session || 0}
                    setValue={(val) => setSession(val)}
                    withButtons
                  />
                </Box>
              </Flex>
            </GridItem>

            <GridItem>
              <Flex align="center" h="full">
                <Heading fontSize="1.2em" color="white">
                  Break:{" "}
                </Heading>
              </Flex>
            </GridItem>
            <GridItem>
              <Flex align="center" h="full">
                <Box p="2" justifyContent="center" alignItems="center">
                  <Num
                    width={20}
                    limits={{ min: 1 }}
                    value={breakTime || 0}
                    setValue={(val) => setBreakTime(val)}
                    withButtons
                  />
                </Box>
              </Flex>
            </GridItem>
          </Grid>
        </Box>
      </form>
    </Header>
  );
};

export default Settings;
