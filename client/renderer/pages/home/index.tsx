import React, { useEffect, useState } from "react";
import { Heading, Flex, Button, Box } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useMeQuery } from "../../generated/graphql";
import { useElectronTime, useTimeContext } from "../_app";
import { Header } from "components/Header";
import { Link } from "chakra-next-link";

const addZero = (number) => (number > 9 ? number : `0${number}`);

const Home = () => {
  const { data, loading } = useMeQuery();
  const router = useRouter();
  const [set, setSet] = useState(false);

  const { time, running, type } = useTimeContext().store;

  const { start, stop, setTimePreference } = useElectronTime();

  useEffect(() => {
    if (!loading && !set) {
      if (data?.me) {
        setSet(true);
        setTimePreference({ work: data.me.timePreference * 60, break: data.me.breakLength * 60 });
      } else {
        router.push("/");
      }
    }
  }, [loading, data]);

  return (
    <Header title={data?.me?.username} selectedIcon="home" isUsername isNotScroll>
      <Link href="/home/activities">Here</Link>
      <Flex w="full" h="full" justifyContent="center" pt="4">
        <Flex>
          <Button
            colorScheme="purple"
            onClick={start}
            isDisabled={running}
            width="160px"
            height="90px"
            borderTopLeftRadius="130px"
            borderTopRightRadius="130px"
            borderBottom="0"
            mt="35px"
            transform="rotate(-90deg)"
            borderBottomLeftRadius="5px"
            borderBottomRightRadius="5px"
          >
            Start
          </Button>
          <Box
            backgroundColor={type === "work" ? "cyan.800" : "yellow.200"}
            height="160px"
            width="160px"
            mx="-6"
            display="flex"
            borderRadius="5px"
            justifyContent="center"
            alignItems="center"
          >
            <Heading color={type === "work" ? "white" : "gray.600"}>
              {addZero(Math.floor(time / 60))}:{addZero(time % 60)}
            </Heading>
          </Box>

          <Button
            colorScheme="teal"
            onClick={stop}
            isDisabled={!running}
            width="160px"
            height="90px"
            borderTopLeftRadius="130px"
            borderTopRightRadius="130px"
            borderBottom="0"
            transform="rotate(90deg)"
            mt="35px"
            borderBottomLeftRadius="5px"
            borderBottomRightRadius="5px"
          >
            Stop
          </Button>
        </Flex>
      </Flex>
    </Header>
  );
};

export default Home;
