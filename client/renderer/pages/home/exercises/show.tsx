import React from "react";
import {
  Spinner,
  Text,
  Heading,
  Box,
  SimpleGrid,
  Stat,
  StatGroup,
  StatLabel,
  StatNumber,
  Link,
} from "@chakra-ui/react";
import { useMeQuery } from "generated/graphql";
import { useRouter } from "next/router";
import { Header } from "components/Header";
import { ExternalLinkIcon } from "@chakra-ui/icons";

const ExercisePage = () => {
  const { data, loading, error } = useMeQuery();
  const { query } = useRouter();
  // @ts-ignore
  const eId = parseInt(query.id || "");

  if (loading) {
    return <Spinner />;
  }

  const exercise = data.me.exercises.find((g) => g.id === eId);

  if (error || !data.me || !exercise) {
    return <Heading>Error...</Heading>;
  }

  return (
    <Box pos="relative">
      <Header title={<Text>{exercise.name}</Text>} selectedIcon="exercise">
        {/* <SimpleGrid columns={2} gap="8" ml="6"> */}
        <Box overflowY="scroll" className="fancyScroll" px="4">
          <Heading fontSize="1em" color="gray.400">
            Description:
          </Heading>

          <Text fontSize=".75em" color="white" maxH="100px">
            {exercise.description}
          </Text>
        </Box>

        {exercise.link && (
          <Box pt="4" pl="4" position="relative">
            <Heading fontSize="1em" color="gray.400">
              Link:
            </Heading>
            <Link href={exercise.link} isExternal color="white" fontSize=".75em">
              {exercise.link} <ExternalLinkIcon mx="2px" color="white" />
            </Link>
          </Box>
        )}
        <Box p="4">
          <Heading fontSize="1em" color="gray.400">
            Created by:
          </Heading>
          <Heading fontSize="1.2em" color="white">
            {data?.me?.username}
          </Heading>
        </Box>
        {/* </SimpleGrid> */}
        {/* <StatGroup
          display="flex"
          color="white"
          backgroundColor="whiteAlpha.100"
          w="full"
          p="4"
          pos="absolute"
          bottom="0"
        >
          <Stat h="full" w="full" display="flex" justifyContent="center" alignItems="center">
            <StatLabel fontWeight="bold">Done</StatLabel>
            <StatNumber>77</StatNumber>
          </Stat>

          <Stat h="full" w="full" display="flex" justifyContent="center" alignItems="center">
            <StatLabel fontWeight="bold">Like</StatLabel>
            <StatNumber>59</StatNumber>
          </Stat>

          <Stat h="full" w="full" display="flex" justifyContent="center" alignItems="center">
            <StatLabel fontWeight="bold">Dislike</StatLabel>
            <StatNumber>4</StatNumber>
          </Stat>
        </StatGroup> */}
      </Header>
    </Box>
  );
};

export default ExercisePage;
