import { DeleteIcon, EditIcon, SmallAddIcon } from "@chakra-ui/icons";
import { Heading, VStack, Text } from "@chakra-ui/layout";
import { HStack, Flex, useToast, SimpleGrid, Badge, Box } from "@chakra-ui/react";
import { Spinner } from "@chakra-ui/spinner";
import { Link, LinkIconButton } from "chakra-next-link";
import { Header } from "components/Header";

import { useDeleteExerciseMutation, useMeQuery } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import { useRouter } from "next/router";

import React from "react";

const ExercisesPage = () => {
  const { data, loading, error } = useMeQuery();

  const router = useRouter();

  const toast = useToast({ status: "success", duration: 1000 });
  const [deleteExercise] = useDeleteExerciseMutation({ refetchQueries: [{ query: ME }] });

  if (loading) {
    return <Spinner />;
  }

  if (error || !data.me) {
    return <Heading>Error...</Heading>;
  }

  const deleteE = async (id) => {
    if (confirm("Are you sure you wont to delete?")) {
      await deleteExercise({ variables: { id: id } });
      toast({ title: "Delete exercise" });
      await router.push("/home/exercises");
    }
  };

  return (
    <Header
      title={
        <HStack spacing="4">
          <Text>My exercises</Text>
        </HStack>
      }
      selectedIcon="exercise"
    >
      <SimpleGrid columns={3} spacing={10} gap="4" py="8" px="10" pos="relative">
        <LinkIconButton
          pos="absolute"
          top="0"
          right="2"
          aria-label="like"
          icon={<SmallAddIcon />}
          size="xs"
          color="white"
          backgroundColor="transparent"
          variant="outline"
          _hover={{ backgroundColor: "orange.300" }}
          _active={{ backgroundColor: "orange" }}
          href="/home/exercises/new"
        />
        {data.me.exercises.map((e) => (
          <VStack
            backgroundColor="whiteAlpha.700"
            color="blue.800"
            height="100px"
            borderRadius="5px"
            fontWeight="bold"
            fontSize="1.4em"
            pos="relative"
          >
            <Box
              display="flex"
              width="100%"
              justifyContent="center"
              backgroundColor="whiteAlpha.400"
              borderTopRightRadius="5px"
              borderTopLeftRadius="5px"
              mb="-0.5rem"
            >
              <Link href={`/home/exercises/show?id=${e.id}`} fontWeight="bold">
                {e.name}
              </Link>
            </Box>
            <HStack spacing="4" h="full" alignItems="center">
              <LinkIconButton
                icon={<EditIcon />}
                aria-label="edit"
                colorScheme="blue"
                size="sm"
                href={`/home/exercises/edit?id=${e.id}`}
              />
              <LinkIconButton
                icon={<DeleteIcon />}
                aria-label="delete"
                colorScheme="red"
                size="sm"
                onClick={() => deleteE(e.id)}
              />
            </HStack>
          </VStack>
          // <Flex
          //   width="80vw"
          //   p="2"
          //   borderRadius="5px"
          //   justify="space-between"
          //   align="center"
          //   backgroundColor="whiteAlpha.700"
          //   color="blue.800"
          // >
          //   <Link href={`/home/exercises/show?id=${e.id}`} fontSize="1.4em" fontWeight="bold">
          //     {e.name}
          //   </Link>

          //   <HStack>
          //     <LinkIconButton
          //       icon={<EditIcon />}
          //       aria-label="edit"
          //       colorScheme="blue"
          //       size="sm"
          //       href={`/home/exercises/edit?id=${e.id}`}
          //     />
          //     <LinkIconButton
          //       icon={<DeleteIcon />}
          //       aria-label="delete"
          //       colorScheme="red"
          //       size="sm"
          //       onClick={() => deleteE(e.id)}
          //     />
          //   </HStack>
          // </Flex>
        ))}
      </SimpleGrid>
    </Header>
  );
};

export default ExercisesPage;
