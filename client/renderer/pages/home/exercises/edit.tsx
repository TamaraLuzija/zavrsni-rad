import React from "react";
import { Spinner, Heading, useToast } from "@chakra-ui/react";
import { useMeQuery, useUpdateExerciseMutation } from "generated/graphql";
import { useRouter } from "next/router";
import { ExerciseForm, ExFormData } from "./new";
import { ME } from "graphql/auth/queries";
import { Header } from "components/Header";

const EditExercisePage = () => {
  const { data, loading, error } = useMeQuery();
  const { query } = useRouter();
  // @ts-ignore
  const eId = parseInt(query.id || "");

  const router = useRouter();
  const [updateExercise] = useUpdateExerciseMutation({ refetchQueries: [{ query: ME }] });

  const toast = useToast({ status: "success", duration: 1000 });

  const onSubmit = async (data: ExFormData) => {
    await updateExercise({ variables: { id: eId, ...data } });
    toast({ title: "Updated an exercise 🥳", duration: 1000 });
    await router.push("/home/exercises");
  };

  if (loading) {
    return <Spinner />;
  }

  const exercise = data.me.exercises.find((g) => g.id === eId);

  if (error || !data.me || !exercise) {
    return <Heading>Error...</Heading>;
  }

  return (
    <Header title={exercise.name} selectedIcon="exercise">
      <ExerciseForm init={exercise} onSubmit={onSubmit} />
    </Header>
  );
};

export default EditExercisePage;
