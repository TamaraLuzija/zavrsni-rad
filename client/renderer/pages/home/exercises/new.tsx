import React, { useState } from "react";
import { Box, Button, Flex, Heading, HStack, useToast, VStack } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { Input, TextArea } from "components/Input";
import { useCreateExerciseMutation } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import { useRouter } from "next/router";
import { Header } from "components/Header";
import { SelectColor } from "components/SelectColors";

export type ExFormData = { name: string; description?: string; link?: string };

export const ExerciseForm: React.FC<{
  init?: ExFormData;
  onSubmit: (data: ExFormData) => Promise<void>;
}> = ({ init, onSubmit }) => {
  const { register, handleSubmit } = useForm({ defaultValues: { ...init } });

  return (
    <Box h="full" w="full" mt="2" overflowY="hidden">
      <form
        onSubmit={handleSubmit((data) => onSubmit({ ...data }))}
        style={{ width: "100%", height: "100%" }}
      >
        <Flex flexDir="column" w="full" h="full" alignItems="center">
          <VStack align="flex-start" w="70%">
            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Name:
              </Heading>
              <Input ref={register} name="name" color="white" noLabel />
            </HStack>
            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Description:
              </Heading>
              <TextArea ref={register} name="description" color="white" noLabel />
            </HStack>

            <HStack w="full">
              <Heading fontSize="1em" w="40%" color="gray.200">
                Link:
              </Heading>
              <Input ref={register} name="link" h="20px" color="white" noLabel />
            </HStack>
          </VStack>
          <Button
            type="submit"
            colorScheme="blue"
            size="md"
            width="20%"
            ml="10"
            mb="4"
            pos="absolute"
            bottom="-2"
            right="8"
          >
            {init?.name ? "Update" : "Create"}
          </Button>
        </Flex>
      </form>
    </Box>
  );
};

const CreateExercisePage = () => {
  const router = useRouter();
  const [createExercise] = useCreateExerciseMutation({ refetchQueries: [{ query: ME }] });

  const toast = useToast({ status: "success", duration: 1000 });

  const onSubmit = async (data: ExFormData) => {
    await createExercise({ variables: { ...data } });
    toast({ title: "Created a new exercise 🥳", duration: 1000 });
    await router.push("/home/exercises");
  };

  return (
    <Header title={"New exercise:"} selectedIcon="exercise" isNotScroll>
      <ExerciseForm onSubmit={onSubmit} />
    </Header>
  );
};

export default CreateExercisePage;
