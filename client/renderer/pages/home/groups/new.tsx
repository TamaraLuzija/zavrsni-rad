import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  useDisclosure,
  Spinner,
  VStack,
} from "@chakra-ui/react";
import { Input } from "components/Input";
import { useCreateGroupMutation, useMeQuery, useUsersQuery } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import { useRouter } from "next/router";
import { Header } from "components/Header";
import Select from "react-select";

interface SelectUserProps {
  excludeIds: number[];
  ids: number[];
  setIds: React.Dispatch<React.SetStateAction<number[]>>;
}

export const SelectUser: React.FC<SelectUserProps> = ({ excludeIds, ids, setIds }) => {
  const { data, error, loading } = useUsersQuery();

  if (loading) return <Spinner />;
  if (error || !data) return <Heading>Error...</Heading>;

  return (
    <Box width="full">
      <Select
        borderColor="gray.600"
        isMulti
        maxMenuHeight={120}
        menuPlacement="bottom"
        value={data.users
          .filter((u) => !excludeIds.includes(u.id))
          .filter((u) => ids.includes(u.id))
          .map((u) => ({ value: u.id, label: u.username }))}
        options={data.users
          .filter((u) => !excludeIds.includes(u.id))
          .map((user) => ({ value: user.id, label: user.username }))}
        onChange={(selected) => {
          setIds(selected.map((v) => v.value));
        }}
      />
    </Box>
  );
};

const NewGroupPage = () => {
  const { data: meData, loading: meLoading, error: meError } = useMeQuery();

  const [name, setName] = useState("");
  const [userIds, setUserIds] = useState<number[]>([]);
  const [createGroup] = useCreateGroupMutation();

  const modalProps = useDisclosure();
  const router = useRouter();

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      await createGroup({
        variables: { name, membersIds: userIds },
        refetchQueries: [{ query: ME }],
        awaitRefetchQueries: true,
      });
      await router.push("/home/groups");
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setName("");
    setUserIds([]);
  }, [modalProps.isOpen]);

  return (
    <Header title="Create group:" selectedIcon="groups">
      <Box h="full" w="full">
        <form onSubmit={onSubmit} style={{ width: "100%", height: "100%" }}>
          <Flex
            flexDir="column"
            justifyContent="space-between"
            w="full"
            h="full"
            alignItems="center"
          >
            <VStack align="flex-start" w="70%" spacing="6" p="2">
              <HStack w="full">
                <Heading fontSize="1em" w="40%" color="gray.200">
                  Group name:
                </Heading>
                <Input
                  isRequired
                  placeholder="Group name"
                  borderColor="gray.600"
                  color="white"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  noLabel
                />
              </HStack>
              <HStack w="full">
                <Heading fontSize="1em" w="40%" color="gray.200">
                  Add members
                </Heading>
                <SelectUser excludeIds={[meData.me.id]} ids={userIds} setIds={setUserIds} />
              </HStack>
            </VStack>

            <Button colorScheme="orange" type="submit" size="md" width="20%" mb="8">
              Create
            </Button>
          </Flex>
        </form>
      </Box>
    </Header>
  );
};

export default NewGroupPage;
