import React, { useState, useMemo } from "react";
import {
  Button,
  Flex,
  Heading,
  HStack,
  IconButton,
  ListItem,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spinner,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  UnorderedList,
  useDisclosure,
  UseDisclosureReturn,
  VStack,
  ModalCloseButton,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { DeleteIcon, SmallAddIcon } from "@chakra-ui/icons";
import { useConfirmDelete } from "chakra-confirm";

import { Header } from "components/Header";
import {
  useInviteToGroupMutation,
  useKickUserMutation,
  useLeaveGroupMutation,
  useDeleteInviteMutation,
  useMeQuery,
} from "generated/graphql";
import { SelectUser } from "pages/home/groups/new";
import { ME } from "graphql/auth/queries";

interface InviteModalProps {
  groupId: number;
  existingUsers: number[];
  modalProps: UseDisclosureReturn;
}

const InviteModal: React.FC<InviteModalProps> = ({ groupId, modalProps, existingUsers }) => {
  const [userIds, setUserIds] = useState<number[]>([]);

  const [inviteToGroup, { loading }] = useInviteToGroupMutation({
    refetchQueries: [{ query: ME }],
    awaitRefetchQueries: true,
  });

  const invite = async () => {
    await inviteToGroup({ variables: { groupId, users: userIds } });
    modalProps.onClose();
  };

  return (
    <Modal {...modalProps}>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalHeader>Invite user</ModalHeader>
        <ModalBody>
          <SelectUser excludeIds={existingUsers} ids={userIds} setIds={setUserIds} />
        </ModalBody>
        <ModalFooter>
          <HStack>
            <Button
              colorScheme="blue"
              onClick={invite}
              isDisabled={userIds.length === 0}
              isLoading={loading}
            >
              Invite
            </Button>
            <Button onClick={modalProps.onClose}>Close</Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

const InvitedPeopleModal: React.FC<{
  canDeleteInvites: boolean;
  modalProps: UseDisclosureReturn;
  invites: { name: string; id: number }[];
  deleteInvite: (inviteId: number) => () => Promise<void>;
}> = ({ canDeleteInvites, modalProps, invites, deleteInvite }) => {
  return (
    <Modal {...modalProps} size="xs">
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalHeader>Invited user</ModalHeader>
        <ModalBody>
          <UnorderedList>
            {invites.map((invite) => (
              <ListItem key={invite.id}>
                <HStack>
                  <Text>{invite.name}</Text>
                  {canDeleteInvites && (
                    <IconButton
                      size="xs"
                      colorScheme="red"
                      icon={<DeleteIcon />}
                      aria-label="delete invite"
                      onClick={deleteInvite(invite.id)}
                    />
                  )}
                </HStack>
              </ListItem>
            ))}
          </UnorderedList>
        </ModalBody>
        <ModalFooter>
          <Button onClick={modalProps.onClose}>Close</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

const GroupPage = () => {
  const { data, loading, error } = useMeQuery();
  const router = useRouter();
  const gId = parseInt((router.query.id as string) || "");

  const [kickUser] = useKickUserMutation();
  const [leaveGroup] = useLeaveGroupMutation();
  const [deleteInvite] = useDeleteInviteMutation();

  const invitedPeopleModalProps = useDisclosure();
  const modalProps = useDisclosure();

  if (loading) {
    return <Spinner />;
  }

  const group = useMemo(() => data.me.groups.find((g) => g.id === gId), [data]);
  if (!group) {
    return null;
  }

  const isUserOwner = group.ownerId === data.me.id;

  if (error || !data.me || !group) {
    return <Heading>Error...</Heading>;
  }

  const confirm = useConfirmDelete();
  const kick = (id: number) => async () => {
    if (
      await confirm({
        title: "Are you sure you want to kick this user",
        body: null,
        buttonText: "Kick",
      })
    ) {
      await kickUser({
        variables: { groupId: gId, userId: id },
        refetchQueries: [{ query: ME }],
        awaitRefetchQueries: true,
      });
    }
  };

  const leave = async () => {
    if (
      await confirm({
        title: "Are you sure you want to leave this group",
        body: null,
        buttonText: "leave",
      })
    ) {
      await leaveGroup({
        variables: { groupId: gId },
        // refetchQueries: [{ query: ME }],
        // awaitRefetchQueries: true,
      });
      await router.push("/home/groups");
    }
  };

  const removeInvite = (id: number) => async () => {
    if (
      await confirm({
        title: "Are you sure you want to delete this invite",
        body: null,
        buttonText: "leave",
      })
    ) {
      await deleteInvite({
        variables: { groupId: gId, inviteId: id },
        refetchQueries: [{ query: ME }],
        awaitRefetchQueries: true,
      });

      invitedPeopleModalProps.onClose();
    }
  };

  const existingUsers = [
    ...group.usersWithRank.map((u) => u.id),
    ...group.invitations.map((inv) => inv.user.id),
  ];

  return (
    <Header title={group.name} selectedIcon="groups">
      <InvitedPeopleModal
        canDeleteInvites={isUserOwner}
        deleteInvite={removeInvite}
        modalProps={invitedPeopleModalProps}
        invites={group.invitations.map((inv) => ({ id: inv.id, name: inv.user.username }))}
      />
      <InviteModal groupId={gId} modalProps={modalProps} existingUsers={existingUsers} />
      <Flex w="full" justify="flex-end" pr="4">
        <HStack>
          {group.invitations.length > 0 && (
            <Button
              size="xs"
              variant="outline"
              color="white"
              _hover={{ backgroundColor: "orange.300" }}
              _active={{ backgroundColor: "orange" }}
              onClick={invitedPeopleModalProps.onOpen}
            >
              Show invited
            </Button>
          )}
          {!isUserOwner && (
            <Button
              size="xs"
              variant="outline"
              color="white"
              _hover={{ backgroundColor: "orange.300" }}
              _active={{ backgroundColor: "orange" }}
              onClick={leave}
            >
              Leave
            </Button>
          )}
          <IconButton
            aria-label="like"
            icon={<SmallAddIcon />}
            size="xs"
            color="white"
            backgroundColor="transparent"
            variant="outline"
            _hover={{ backgroundColor: "orange.300" }}
            _active={{ backgroundColor: "orange" }}
            onClick={modalProps.onOpen}
          />
        </HStack>
      </Flex>
      <VStack p="4" align="flex-start">
        <Table height="100%" variant="striped" backgroundColor="gray.200">
          <Thead>
            <Tr>
              <Th>Rang</Th>
              <Th>Members</Th>
              <Th isNumeric>Work (h)</Th>
              <Th isNumeric>Break (h)</Th>
              {isUserOwner && <Th>Ops</Th>}
            </Tr>
          </Thead>

          <Tbody>
            {group.usersWithRank.map((u) => (
              <Tr key={u.id} w="full">
                <Td>{u.rank}</Td>
                <Td>{u.username}</Td>

                <Td isNumeric>{u.stats.work / 60}h</Td>
                <Td isNumeric>{u.stats.break / 60}h</Td>
                {isUserOwner && (
                  <Td>
                    {u.id !== data.me.id && (
                      <IconButton
                        size="xs"
                        colorScheme="red"
                        icon={<DeleteIcon />}
                        aria-label="kick"
                        onClick={kick(u.id)}
                      />
                    )}
                  </Td>
                )}
              </Tr>
            ))}
          </Tbody>
        </Table>
      </VStack>
    </Header>
  );
};

export default GroupPage;
