import React from "react";
import { SmallAddIcon } from "@chakra-ui/icons";
import { Heading, VStack } from "@chakra-ui/layout";
import { Badge, Box, HStack, SimpleGrid } from "@chakra-ui/react";
import { Spinner } from "@chakra-ui/spinner";
import { Link, LinkIconButton } from "chakra-next-link";
import { Header } from "components/Header";
import { useMeQuery } from "generated/graphql";

const GroupsPage = () => {
  const { data, loading, error } = useMeQuery();

  if (loading) {
    return <Spinner />;
  }

  if (error || !data.me) {
    return <Heading>Error...</Heading>;
  }

  return (
    <Header title="My groups" selectedIcon="groups">
      <SimpleGrid columns={3} spacing={10} gap="4" py="8" px="10" pos="relative">
        <LinkIconButton
          pos="absolute"
          top="0"
          right="2"
          aria-label="like"
          icon={<SmallAddIcon />}
          size="xs"
          color="white"
          backgroundColor="transparent"
          variant="outline"
          _hover={{ backgroundColor: "orange.300" }}
          _active={{ backgroundColor: "orange" }}
          href="/home/groups/new"
        />
        {data.me.groups.map((g) => (
          <VStack
            backgroundColor="whiteAlpha.700"
            color="blue.800"
            borderRadius="5px"
            fontWeight="bold"
            fontSize="1.4em"
            pb="4"
            key={g.id}
          >
            <Box
              display="flex"
              width="100%"
              justifyContent="center"
              backgroundColor="whiteAlpha.400"
              borderTopRightRadius="5px"
              borderTopLeftRadius="5px"
            >
              <Link href={`/home/groups/show?id=${g.id}`}>{g.name}</Link>
            </Box>

            <HStack>
              <Heading fontSize="0.8em">Rang:</Heading>
              <Badge colorScheme="blue" textAlign="center" px="3">
                {g.usersWithRank.find((u) => u.id === data.me.id).rank || -1}.
              </Badge>
            </HStack>
          </VStack>
        ))}
      </SimpleGrid>
    </Header>
  );
};

export default GroupsPage;
