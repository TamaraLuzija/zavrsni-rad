import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Heading,
  HStack,
  IconButton,
  Modal,
  Text,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  SimpleGrid,
  useToast,
  VStack,
  Link,
  ListItem,
  OrderedList,
} from "@chakra-ui/react";
import { CheckIcon, CloseIcon } from "@chakra-ui/icons";
import { useRouter } from "next/router";

import { MeQuery, useMeQuery, useRespondToInviteMutation } from "generated/graphql";
import { ME } from "graphql/auth/queries";
import { Header } from "components/Header";

type Invite = MeQuery["me"]["invitations"][number];

interface PreviewModalProps {
  onClose: () => void;
  isOpen: boolean;
  invite?: Invite;

  join: () => void;
  decline: () => void;
}

const PreviewModal: React.FC<PreviewModalProps> = ({ onClose, isOpen, invite, join, decline }) => {
  return (
    <Modal onClose={onClose} isOpen={isOpen}>
      <ModalOverlay />
      {invite && (
        <ModalContent>
          <ModalCloseButton />
          <ModalHeader>Invitation: {invite.group.name}</ModalHeader>
          <ModalBody>
            <VStack align="flex-start">
              <Box>
                <Heading size="md" d="inline">
                  Owner:{" "}
                </Heading>
                <Text d="inline">{invite.group.owner.username}</Text>
              </Box>

              <OrderedList pl="5">
                {invite.group.usersWithRank.map((user) => (
                  <ListItem key={user.id}>{user.username}</ListItem>
                ))}
              </OrderedList>
            </VStack>
          </ModalBody>

          <ModalFooter>
            <HStack>
              <Button onClick={onClose}>Close</Button>
              <Button onClick={join} colorScheme="green">
                Join
              </Button>
              <Button onClick={decline} colorScheme="red">
                Decline
              </Button>
            </HStack>
          </ModalFooter>
        </ModalContent>
      )}
    </Modal>
  );
};

const Notifications = () => {
  const { data, loading } = useMeQuery();
  const router = useRouter();

  const [loadingAccept, setLoadingAccept] = useState<number[]>([]);
  const [loadingDecline, setLoadingDecline] = useState<number[]>([]);

  const [respondToInvite] = useRespondToInviteMutation();
  const toast = useToast({ status: "success", duration: 1000 });

  const [invite, setInvite] = useState<Invite>(undefined);

  const onSubmit = (inviteId: number, groupName: string, accept: boolean) => async () => {
    if (accept) {
      setLoadingAccept((l) => [...l, inviteId]);
    } else {
      setLoadingDecline((l) => [...l, inviteId]);
    }

    try {
      const res = await respondToInvite({
        variables: { inviteId, accept },
        refetchQueries: [{ query: ME }],
        awaitRefetchQueries: true,
      });
      if (res) {
        if (accept) {
          toast({ title: `Successfully joined ${groupName}` });
        } else {
          toast({ title: `Rejected ${groupName}`, status: "warning" });
        }

        // await router.push("/home");
      }
    } catch (err) {
      console.log(err);
    } finally {
      if (accept) {
        setLoadingAccept((l) => l.filter((id) => id !== inviteId));
      } else {
        setLoadingDecline((l) => l.filter((id) => id !== inviteId));
      }
    }
  };

  useEffect(() => {
    if (!loading) {
      if (!data.me) {
        router.push("/login");
      }
    }
  }, [loading, data]);

  return (
    <Header title="Group invites" selectedIcon="notification">
      <PreviewModal
        isOpen={!!invite}
        onClose={() => setInvite(undefined)}
        invite={invite}
        join={() => {
          if (invite) {
            onSubmit(invite.id, invite.group.name, true)();
            setInvite(undefined);
          }
        }}
        decline={() => {
          if (invite) {
            onSubmit(invite.id, invite.group.name, false)();
            setInvite(undefined);
          }
        }}
      />
      <SimpleGrid columns={3} spacing={10} gap="4" py="8" px="10" pos="relative">
        {data?.me.invitations.length > 0 ? (
          data?.me.invitations.map((i) => (
            <VStack
              key={i.id}
              backgroundColor="whiteAlpha.700"
              color="blue.800"
              height="100px"
              borderRadius="5px"
              fontWeight="bold"
              fontSize="1.4em"
              pos="relative"
            >
              <Box
                display="flex"
                width="100%"
                justifyContent="center"
                backgroundColor="whiteAlpha.400"
                borderTopRightRadius="5px"
                borderTopLeftRadius="5px"
                mb="-0.5rem"
              >
                <Link fontWeight="bold" onClick={() => setInvite(i)}>
                  {i.group.name}
                </Link>
                {/* <Heading fontSize="1.4em">{i.group.name}</Heading> */}
                {/* <Heading fontSize=".8em">by {i.group.owner.username}</Heading> */}
              </Box>

              <HStack spacing="4" h="full" alignItems="center">
                <IconButton
                  aria-label="accept"
                  icon={<CheckIcon />}
                  isLoading={loadingAccept.includes(i.id)}
                  isDisabled={loadingDecline.includes(i.id)}
                  colorScheme="green"
                  size="sm"
                  onClick={onSubmit(i.id, i.group.name, true)}
                />
                {/* Accept
                </IconB> */}
                <IconButton
                  aria-label="decline"
                  icon={<CloseIcon />}
                  isLoading={loadingDecline.includes(i.id)}
                  isDisabled={loadingAccept.includes(i.id)}
                  colorScheme="red"
                  size="sm"
                  onClick={onSubmit(i.id, i.group.name, false)}
                />
              </HStack>
            </VStack>
          ))
        ) : (
          <Heading gridColumn={2} color="gray.100" w="full" display="flex" m="0 auto">
            No invites 😢
          </Heading>
        )}
      </SimpleGrid>
    </Header>
  );
};

export default Notifications;
