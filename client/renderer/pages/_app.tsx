import React, { createContext, Dispatch, useContext, useEffect, useReducer, useRef } from "react";
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from "@apollo/client";
import { ChakraProvider, Heading, Spinner } from "@chakra-ui/react";
import { setContext } from "@apollo/client/link/context";
import produce from "immer";
import Head from "next/head";
import { useRouter } from "next/router";
import { ConfirmContextProvider } from "chakra-confirm";

import { useElectron } from "../lib/electron";
import "../style/home.css";
import "../style/questions.css";
import {
  MeQuery,
  useCreateActivityMutation,
  useMeQuery,
  useUpdateActivityMutation,
} from "generated/graphql";
import { ME } from "graphql/auth/queries";

const httpLink = createHttpLink({ uri: "http://localhost:4000/graphql" });
const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("token");

  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({
    typePolicies: {
      Group: {
        fields: {
          invitations: { merge: (_curr, newData) => newData },
        },
      },
      User: {
        fields: {
          invitations: { merge: (_curr, newData) => newData },
          groups: { merge: (_curr, newData) => newData },
        },
      },
      UserWithRank: {
        keyFields: ["id", "groupId"],
      },
    },
  }),
});

interface TimeContextStore {
  time: number;
  running: boolean;
  type: "work" | "break";
}

export const timeActions = {
  setType: (s: TimeContextStore["type"]) => ({ type: "setType", payload: s } as const),
  setRun: (run: boolean) => ({ type: "setRun", payload: run } as const),
  setTimePreference: (t: number) => ({ type: "setTime", payload: t } as const),
  resetTime: () => ({ type: "resetTime" } as const),
};

type TimeContextAction =
  | ReturnType<typeof timeActions.setRun>
  | ReturnType<typeof timeActions.setTimePreference>
  | ReturnType<typeof timeActions.resetTime>
  | ReturnType<typeof timeActions.setType>;
type TimeContextType = { store: TimeContextStore; dispatch: Dispatch<TimeContextAction> };

const timeReducer = (state: TimeContextStore, action: TimeContextAction): TimeContextStore => {
  switch (action.type) {
    case "setRun":
      return { ...state, running: action.payload };
    case "setTime":
      return produce(state, (draft) => {
        draft.time = action.payload;
      });
    case "setType":
      return { ...state, type: action.payload };
    default:
      return state;
  }
};

const TimeContext = createContext<TimeContextType>({
  store: { time: 0, running: false, type: "work" },
  dispatch: () => {},
});
export const useTimeContext = () => useContext(TimeContext);

export const useElectronTime = () => {
  const ipc = useElectron();

  return {
    start: () => ipc.send("startTimer"),
    stop: () => ipc.send("stopTimer"),
    setTimePreference: (data: any) => ipc.send("setTimePreference", data),
  };
};

type TickType = {
  running: boolean;
  time: number;
  type: TimeContextStore["type"];
  event: {
    type: "ping" | "finalPing" | "change";
    data: {
      type: TimeContextStore["type"];
      fullTime: number;
      value: number;
    };
  } | null;
};

const BaseApp: React.FC<{ me: MeQuery }> = ({ children, me }) => {
  const [store, dispatch] = useReducer(timeReducer, { time: 0, running: false, type: "work" });
  const activityId = useRef<number | undefined>(undefined);

  const refetchOptions = { refetchQueries: [{ query: ME }], awaitRefetchQueries: true };

  const [createActivity] = useCreateActivityMutation(refetchOptions);
  const [updateActivity] = useUpdateActivityMutation(refetchOptions);

  const ipc = useElectron();
  useEffect(() => {
    // @ts-ignore
    window.ipc = ipc;

    if (me.me?.typeOfExercise === "exercise") {
      ipc.send(
        "setExercises",
        me.me?.exercises.map((et) => et.name)
      );
    } else {
      ipc.send("setExercises", undefined);
    }
  }, [me.me?.typeOfExercise]);

  useEffect(() => {
    ipc.on("tick", async (event, data: TickType) => {
      dispatch(timeActions.setTimePreference(data.time));
      dispatch(timeActions.setType(data.type));

      console.log(activityId, data);

      if (data.running && (data.event || activityId.current === undefined)) {
        if (data.event?.type === "finalPing") {
          await updateActivity({
            variables: {
              isComplete: true,
              id: activityId.current,
              time: data.event.data.fullTime,
              fullTime: data.event.data.fullTime,
            },
          });

          activityId.current = undefined;
          return;
        }

        if (data?.event?.type === "change") {
          console.log("change");
        }

        switch (activityId.current) {
          case undefined:
            activityId.current = -1;
            console.log("Creating");
            const res = await createActivity({
              variables: {
                fullTime: data.event?.data.fullTime || -1,
                isBreak: data.type === "break",
              },
            });
            activityId.current = res.data.createActivity.id;
            break;
          case -1:
            console.log("Skip", data);
            break;
          default:
            console.log("Updating");
            await updateActivity({
              variables: {
                id: activityId.current,
                time: data.event.data.value,
                fullTime: data.event.data.fullTime,
              },
            });
        }
      }
    });

    ipc.on("state", (event, data: boolean) => {
      dispatch(timeActions.setRun(data));
    });

    ipc.on("timerType", (event, data) => {
      console.log(data);
    });

    ipc.send("getTimer");
  }, [ipc]);

  return (
    <TimeContext.Provider value={{ store, dispatch }}>
      <Head>
        <title>Standy</title>
      </Head>
      {children}
    </TimeContext.Provider>
  );
};

const loggedOutRoutes = ["/login", "/register", "/"];

const ApolloWrappedApp: React.FC = ({ children }) => {
  const skip = typeof window !== "undefined" ? !window.localStorage.getItem("token") : false;
  const router = useRouter();
  const { loading, error, data } = useMeQuery({ skip });

  // useEffect(() => {
  //   if (skip && !loading) {
  //     router.push("/");
  //   }

  //   if (!loading && data && data.me === null) {
  //     localStorage.removeItem("token");
  //     router.push("/");
  //   }
  // }, [loading, skip, data]);

  if (skip) {
    return null;
  }

  if (loading) {
    return <Spinner size="xl" />;
  }

  if (error) {
    localStorage.removeItem("token");
    router.push("/");
    return null;
  }

  return <BaseApp me={data}>{children}</BaseApp>;
};

const Wrapped: React.FC = ({ children }) => {
  const router = useRouter();
  if (loggedOutRoutes.includes(router.route)) {
    return (
      <>
        <Head>
          <title>Standy</title>
        </Head>
        {children}
      </>
    );
  }

  return <ApolloWrappedApp>{children}</ApolloWrappedApp>;
};

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider>
        <ConfirmContextProvider>
          <Wrapped>
            <Component {...pageProps} />
          </Wrapped>
        </ConfirmContextProvider>
      </ChakraProvider>
    </ApolloProvider>
  );
}

export default MyApp;
