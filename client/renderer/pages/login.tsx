import React, { useState } from "react";
import { Box, Heading, VStack } from "@chakra-ui/layout";
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button,
  CloseButton,
  HStack,
  Text,
} from "@chakra-ui/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { Input } from "../components/Input";
import { useSignInMutation } from "../generated/graphql";
import { useApolloClient } from "@apollo/client";
import { ME } from "graphql/auth/queries";

interface FormData {
  email: string;
  password: string;
}

const LoginPage = () => {
  const router = useRouter();
  const [globalError, setGlobalError] = useState<any>();
  const { register, handleSubmit, errors, formState } = useForm<FormData>({
    defaultValues: {},
  });

  const [signIn] = useSignInMutation();
  const client = useApolloClient();

  const onSubmit = async (data: FormData) => {
    try {
      localStorage.removeItem("token");
      const res = await signIn({ variables: { ...data } });
      const { token } = res.data.signIn;

      localStorage.setItem("token", token);
      await client.resetStore();
      await client.query({ query: ME });
      await router.push("/home");
    } catch (error) {
      setGlobalError("Unknown error occurred");
      console.error(error);
    }
  };

  return (
    <>
      <Box w="100vw" h="100vh" backgroundColor="gray.700" display="flex" m="0">
        <HStack>
          <Heading fontSize="3em" color="white" textAlign="center" transform="rotate(-90deg)">
            Login
          </Heading>
          <VStack spacing="3">
            <Box
              width="400px"
              p="2"
              backgroundColor="purple.50"
              borderRadius="5px"
              position="relative"
            >
              {globalError && (
                <Alert status="error">
                  <AlertIcon />
                  <AlertTitle mr={2}>Error occurred</AlertTitle>
                  <AlertDescription>{globalError}</AlertDescription>
                  <CloseButton
                    onClick={() => setGlobalError(undefined)}
                    position="absolute"
                    right="8px"
                    top="8px"
                  />
                </Alert>
              )}
              <form onSubmit={handleSubmit(onSubmit)}>
                <VStack mx="4" spacing="4">
                  <VStack w="full">
                    <Input
                      error={errors["email"]?.message}
                      ref={register()}
                      name="email"
                      color="teal.800"
                    />
                    <Input error={errors["password"]?.message} ref={register()} name="password" />
                  </VStack>

                  <Button
                    type="submit"
                    colorScheme="teal"
                    px="14"
                    isLoading={formState.isSubmitting}
                  >
                    Log in
                  </Button>
                  <Text>
                    Don't have an Account? <Link href="/register">Register here</Link>
                  </Text>
                </VStack>
              </form>
            </Box>
          </VStack>
        </HStack>
      </Box>
    </>
  );
};

export default LoginPage;
