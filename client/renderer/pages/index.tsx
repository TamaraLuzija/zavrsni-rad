import React, { useEffect, useState } from "react";
import { Box, Heading, VStack } from "@chakra-ui/layout";
import { LinkButton } from "chakra-next-link";
import { useMeQuery } from "../generated/graphql";
import { useRouter } from "next/router";

const FirstPage = () => {
  const { data, loading } = useMeQuery();
  const router = useRouter();

  const [first, other] = useState(false);
  useEffect(() => {
    other(true);
  }, []);

  useEffect(() => {
    if (!loading && data?.me) {
      router.push("/home");
    }
  }, [loading, data]);

  return (
    <>
      <Box
        pos="relative"
        d="flex"
        justifyContent="center"
        alignItems="center"
        w="100vw"
        h="100vh"
        className={"box"}
        backgroundColor="gray.100"
      >
        <Box
          w="80vw"
          h="80vh"
          className={`hid-box ${first ? " move" : ""}`}
          borderRadius="5px"
          backgroundColor="gray.700"
          display="flex"
          justifyContent="center"
        >
          <VStack spacing="4" mt="4">
            <Heading as="h1" fontSize={"5em"} color="white">
              Standy
            </Heading>

            <LinkButton
              href="/login"
              px="16"
              py="6"
              variant="outline"
              borderWidth="2px"
              color="white"
              _hover={{ backgroundColor: "whiteAlpha.400" }}
              _active={{ backgroundColor: "whiteAlpha.600" }}
              fontSize="1.2em"
              fontWeight="bold"
            >
              Start
            </LinkButton>
          </VStack>
        </Box>
      </Box>
    </>
  );
};

export default FirstPage;
