import { gql } from "@apollo/client";

export const CREATE_GROUP = gql`
  mutation createGroup($name: String!, $membersIds: [Int!]!) {
    createGroup(data: { name: $name, memberIds: $membersIds }) {
      id
      name
      usersWithRank {
        id
        groupId
        username
      }
      invitations {
        id
        user {
          id
          username
        }
      }
    }
  }
`;

export const RESPOND_TO_INVITE = gql`
  mutation respondToInvite($inviteId: Int!, $accept: Boolean!) {
    respondToInvite(inviteId: $inviteId, accept: $accept)
  }
`;

export const INVITE_TO_GROUP = gql`
  mutation inviteToGroup($groupId: Int!, $users: [Int!]!) {
    inviteToGroup(id: $groupId, userIds: $users) {
      id
      invitations {
        id
        user {
          id
          username
        }
      }
    }
  }
`;

export const KICK_USER = gql`
  mutation kickUser($groupId: Int!, $userId: Int!) {
    kickUser(groupId: $groupId, userId: $userId) {
      id
      usersWithRank {
        id
        groupId
        username
      }
    }
  }
`;

export const LEAVE_GROUP = gql`
  mutation leaveGroup($groupId: Int!) {
    leave(groupId: $groupId) {
      id
      groups {
        id
        name
        ownerId
        usersWithRank {
          id
          groupId
          rank
          username
          stats {
            work
            break
          }
        }
        invitations(joined: false) {
          id
          user {
            id
            username
          }
        }
      }
    }
  }
`;

export const DELETE_INVITE = gql`
  mutation deleteInvite($groupId: Int!, $inviteId: Int!) {
    deleteInvite(groupId: $groupId, inviteId: $inviteId) {
      id
      invitations {
        id
        groupId
      }
    }
  }
`;
