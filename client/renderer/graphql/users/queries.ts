import { gql } from "@apollo/client";

export const USERS = gql`
  query USERS {
    users {
      id
      username
    }
  }
`;

export const stats = gql`
  query Stats {
    profileStats {
      work {
        now
        before

        countNow
        countBefore
      }

      break {
        now
        before

        countNow
        countBefore
      }

      diff
      diffBefore
    }
  }
`;
