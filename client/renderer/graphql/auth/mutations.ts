import { gql } from "@apollo/client";

export const SIGN_IN = gql`
  mutation signIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
      user {
        id
        username
      }
    }
  }
`;

export const SIGN_UP = gql`
  mutation signUp($email: String!, $username: String!, $password: String!) {
    signUp(data: { email: $email, username: $username, password: $password }) {
      token
      user {
        id
        username
      }
    }
  }
`;
