import { gql } from "@apollo/client";

export const ME = gql`
  query ME {
    me {
      id
      email
      username
      workHours
      workplace
      typeOfExercise
      timePreference
      breakLength
      exercises {
        id
        name
        description
        link
      }
      invitations(joined: false) {
        id
        group {
          id
          name
          owner {
            id
            username
          }
          usersWithRank {
            id
            groupId
            username
          }
        }
      }
      groups {
        id
        name
        ownerId
        usersWithRank {
          id
          groupId
          rank
          username
          stats {
            work
            break
          }
        }
        invitations(joined: false) {
          id
          user {
            id
            username
          }
        }
      }
    }
  }
`;
