import { gql } from "@apollo/client";
export const GOALS = gql`
  query GOALS {
    userAchievementType {
      types {
        id
        name
        value
        type
        typeName
      }
      userCompletedTypes {
        type
        value
      }
      allTypes {
        type
        value
      }
    }
  }
`;

export const UserAchievements = gql`
  query UserAchievements {
    userAchievements {
      id
      name
      maxLevel
      userLevel
      message
      currentValue
      nextValue
    }
  }
`;
