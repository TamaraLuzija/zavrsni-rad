import { gql } from "@apollo/client";

export const QUESTIONS = gql`
  mutation questions($timePreference: Int, $breakLength: Int, $typeOfExercise: String) {
    updateQuestions(
      data: {
        timePreference: $timePreference
        breakLength: $breakLength
        typeOfExercise: $typeOfExercise
      }
    ) {
      id
      timePreference
      breakLength
      typeOfExercise
    }
  }
`;
