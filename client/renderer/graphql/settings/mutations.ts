import { gql } from "@apollo/client";

export const TIME_PREFERENCE = gql`
  mutation updateTimePreference($time: Int, $breakLength: Int, $type: String) {
    updateTime(data: { timePreference: $time, breakLength: $breakLength }) {
      id
      timePreference
      breakLength
    }
    updateQuestions(data: { typeOfExercise: $type }) {
      id
      typeOfExercise
    }
  }
`;
