import gql from "graphql-tag";

export const createExercise = gql`
  mutation createExercise($name: String!, $description: String, $link: String) {
    createExercise(data: { name: $name, description: $description, link: $link }) {
      id
      name
      description
      link
    }
  }
`;

export const updateExercise = gql`
  mutation updateExercise($id: Int!, $name: String, $description: String, $link: String) {
    updateExercise(id: $id, data: { name: $name, description: $description, link: $link }) {
      id
      name
      description
      link
    }
  }
`;

export const deleteExercise = gql`
  mutation deleteExercise($id: Int!) {
    deleteExercise(id: $id) {
      id
      name
    }
  }
`;

export const createActivity = gql`
  mutation createActivity($isBreak: Boolean!, $fullTime: Int!) {
    createActivity(isBreak: $isBreak, fullTime: $fullTime) {
      id
      complete
      isBreak
      time
    }
  }
`;

export const updateActivity = gql`
  mutation updateActivity($id: Int!, $time: Int, $isComplete: Boolean, $fullTime: Int) {
    updateActivity(id: $id, data: { time: $time, isComplete: $isComplete, fullTime: $fullTime }) {
      id
      complete
      isBreak
      time
    }
  }
`;

export const Activities = gql`
  query Activities {
    activities {
      id
      complete
      isBreak
      time
      fullTime
      date
      exercise {
        id
        name
      }
    }
  }
`;
